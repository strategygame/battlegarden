﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 건물 정보 구조체
 * */
public struct TowerInfoStruct
{
    public float maxHealthPoint, healthPoint;
    public bool canAttack;
    public float attackPoint, attackRange, attackSpeed;
    public float defensePoint;
    public AttackType attackType;
    public int buildCost, maintenanceCost;
    public int level;
    public int levelUpCost, levelUpAddMaintenanceCost;
    public int spawnUnitAmount;
}

/* 유닛 정보 구조체
 * */
public struct UnitInfoStruct
{
    public float maxHealthPoint, healthPoint;
    public float moveSpeed;
    public float attackSpeed, attackPoint, attackRange;
    public float defensePoint;
    public float sightRange;
    public float buffScale;
    public bool canBuff;
    public bool hasSplashZone;
    public int level;
    public AttackPriority attackPriority;
    public AttackType attackType;
    public UnitType unitType;
}

public struct ItemInfoStruct
{
    public int itemNo;
    public string name;
    public string description;
    public Sprite icon;
    public string towerID;
    public int level;
    public int maxLevelExp, currentLevelExp;
    public int grade;
    public string gradeText;
    public int maxGradeExp, currentGradeExp;
    public int provideLevelExp, provideGradeExp;
}