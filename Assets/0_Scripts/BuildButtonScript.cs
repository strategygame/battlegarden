﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildButtonScript : MonoBehaviour
{
    public Team team { get; private set; }
    public int buttonIndex;
    public int buildCost { get; private set; }

    public void Init(Team team, int buildCost)
    {
        this.team = team;
        this.buildCost = buildCost;
    }
}
