﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerScript : MonoBehaviour
{
    public const int MaxLevel = 5;

    private SpriteRenderer mySpriteRenderer;
    private ObjectInfo myObjectInfo;

    [SerializeField]
    private GameObject unitPrefab;
    [SerializeField]
    private GameObject projectile;
    [SerializeField]
    private RangeScript attackRader;
    [SerializeField]
    private Button UpgradeButton;

    public Team team { get; private set; }
    [SerializeField]
    private string towerKey;
    private TowerType towerType;
    private bool buffed;
    private TowerInfoStruct initInfo = new TowerInfoStruct();
    private TowerInfoStruct currentInfo = new TowerInfoStruct();
    public int[] objectPoolIndex { get; private set; }
    private Vector2 towerDicionaryIndex = new Vector2();


	// Use this for initialization
	void Awake ()
    {
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        myObjectInfo = GetComponent<ObjectInfo>();
        if(team == Team.BTeam)
        {
            UpgradeButton.gameObject.SetActive(false);
        }
        UpgradeButton.onClick.AddListener(OnUpgradeButtonClicked);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // 오브젝트 활성화시 초기화
    void OnEnable()
    {
        currentInfo = initInfo;
    }

    // 최초 초기화
    public void Init(Team team, int[] objectPoolIndex)
    {
        this.team = team;
        Singleton<JSonManager>.Instance.GetTowerInfoStruct(ref initInfo, towerKey, 0);
        currentInfo = initInfo;
        buffed = false;
        this.objectPoolIndex = objectPoolIndex;
        myObjectInfo.team = team;
        myObjectInfo.isUnit = false;
    }

    public GameObject GetUnitPrefab()
    {
        return unitPrefab;
    }

    public void SetPosition(Vector2 newPosition)
    {
        transform.position = new Vector3(newPosition.x, newPosition.y + 0.1f);
        myObjectInfo.gridPosition = new Vector2((int)(newPosition.x * 10), (int)(newPosition.y * 10));
        towerDicionaryIndex = newPosition;
    }

    public int GetBuildCost()
    {
        Debug.Log("레벨 : " + currentInfo.level);
        return initInfo.buildCost;
    }

    public void SpawnUnit()
    {
        if (towerType != TowerType.방어타워 && towerType != TowerType.넥서스)
        {
            for (int i = 0; i < currentInfo.spawnUnitAmount; i++)
            {
                GameObject spawnedUnit = Singleton<ObjectPoolScript>.Instance.SpawnUnitObject(objectPoolIndex[0], team);
                spawnedUnit.GetComponent<UnitScript>().InitSpawned(currentInfo.level, new Vector2(transform.position.x, transform.position.y - 0.1f));
            }
        }
    }

    /* 공격을 받아 데미지를 입을 때 실행.
     * 체력이 0 이하가 되면 사망처리.
     * */
    public void GetDamage(float damagePoint)
    {
        if(damagePoint - currentInfo.defensePoint >= 1)
        {
            currentInfo.healthPoint -= damagePoint - currentInfo.defensePoint;
        }
        else
        {
            currentInfo.healthPoint -= 1;
        }

        if(currentInfo.healthPoint <= 0)
        {
            Singleton<ObjectPoolScript>.Instance.DeSpawnTowerObject(objectPoolIndex, team);
            Singleton<LevelManager>.Instance.towerDictionary.Remove(towerDicionaryIndex);
            Singleton<LevelManager>.Instance.towerDictionaryIndex.Remove(towerDicionaryIndex);

            gameObject.SetActive(false);

            Singleton<LevelManager>.Instance.UpdateTileState(new Vector2((int)transform.position.x, (int)transform.position.y), TileState.CanBuildTower);
            if(team == Team.BTeam)
            {
                Singleton<AIScript>.Instance.DestroyedTower(myObjectInfo);
            }
        }
    }

    public int GetUpgradeCost()
    {
        return currentInfo.levelUpCost;
    }

    public int GetTowerLevel()
    {
        return currentInfo.level;
    }

    public void UpgradeTower()
    {
        Singleton<JSonManager>.Instance.GetTowerInfoStruct(ref currentInfo, towerKey, currentInfo.level);
    }

    private void OnUpgradeButtonClicked()
    {
        if(Singleton<CostManager>.Instance.GetMoney(team) >= currentInfo.levelUpCost && currentInfo.level < MaxLevel)
        {
            Singleton<CostManager>.Instance.AddMoney(-GetUpgradeCost(), team);
            UpgradeTower();
        }
    }

    public TowerType GetTowerType()
    {
        return towerType;
    }
}
