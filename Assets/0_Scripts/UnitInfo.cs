﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 유닛의 정보를 가지고 있는 구조체 클래스
 * */
public class UnitInfo
{
    public float healthPoint { get; set; }
    public float moveSpeed { get; private set; }
    public float attackSpeed { get; set; }
    public float attackPoint { get; private set; }
    public float defensePoint { get; private set; }
    public float attackRange { get; private set; }
    public float sightRange { get; private set; }
    public bool canBuff { get; private set; }
    public float buffScale { get; private set; }
    public int unitLevel { get; private set; }
    public AttackPriority attackPriority { get; private set; }
    public AttackType attackType { get; private set; }

    
    /* 유닛의 정보를 초기화.
     * 나중엔 JSon파일에서 읽어오는 형식으로 변경할 것.
     * */
    public UnitInfo(UnitType unitType, int level)
    {
        if (unitType == UnitType.전사) // 근접
        {
            unitLevel = level;
            healthPoint = 10;
            moveSpeed = 1.1f;
            attackSpeed = 1;
            attackPoint = 3;
            defensePoint = 0;
            attackRange = 1 * Mathf.Sqrt(5) / 3;
            sightRange = 5;
            attackPriority = AttackPriority.Default;
            attackType = AttackType.근접단일;
            canBuff = false;
            buffScale = 0;
        }
        else if (unitType == UnitType.마법사) // 법사
        {
            unitLevel = level;
            healthPoint = 4.5f;
            moveSpeed = 1.0f;
            attackSpeed = 0.8f;
            attackPoint = 2;
            defensePoint = 0;
            attackRange = 2.5f * Mathf.Sqrt(5) / 3;
            sightRange = 5;
            attackPriority = AttackPriority.Default;
            attackType = AttackType.발사체범위;
            canBuff = false;
            buffScale = 0;
        }
        else if (unitType == UnitType.궁수) // 궁수
        {
            unitLevel = level;
            healthPoint = 3;
            moveSpeed = 0.9f;
            attackSpeed = 1;
            attackPoint = 4;
            defensePoint = 0;
            attackRange = 4 * Mathf.Sqrt(5) / 3;
            sightRange = 5;
            attackPriority = AttackPriority.Default;
            attackType = AttackType.발사체;
            canBuff = false;
            buffScale = 0;
        }
        else if (unitType == UnitType.힐러) //힐러
        {
            unitLevel = level;
            healthPoint = 5;
            moveSpeed = 1.3f;
            attackSpeed = 1;
            attackPoint = 1.5f;
            defensePoint = 0;
            attackRange = 3 * Mathf.Sqrt(5) / 3;
            sightRange = 5;
            attackPriority = AttackPriority.Default;
            attackType = AttackType.힐;
            canBuff = false;
            buffScale = 0;
        }
        else if (unitType == UnitType.버퍼) // 버퍼
        {
            unitLevel = level;
            healthPoint = 5;
            moveSpeed = 1.3f;
            attackSpeed = 1;
            attackPoint = 1;
            defensePoint = 0;
            attackRange = 3 * Mathf.Sqrt(5) / 3;
            sightRange = 5;
            attackPriority = AttackPriority.Default;
            attackType = AttackType.발사체;
            canBuff = true;
            buffScale = 10.0f / 11.0f;
        }
    }
}
