﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainUIManager : MonoBehaviour
{
    [SerializeField]
    private List<Button> mainButtons;
    private List<MainButtonScript> buttonIndexs = new List<MainButtonScript>();
    [SerializeField]
    private GameObject InventoryPannel;
    [SerializeField]
    private Text goldText;
    [SerializeField]
    private Text JemText;

    private MainPannel currentOpenPannel;

    void Awake()
    {
        for(int i = 0; i < mainButtons.Count; i++)
        {
            buttonIndexs.Add(mainButtons[i].GetComponent<MainButtonScript>());
        }
        mainButtons[0].onClick.AddListener(() => OnButtonClicked(buttonIndexs[0].buttonIndex));
        mainButtons[1].onClick.AddListener(() => OnButtonClicked(buttonIndexs[1].buttonIndex));
        mainButtons[2].onClick.AddListener(() => OnButtonClicked(buttonIndexs[2].buttonIndex));
        mainButtons[3].onClick.AddListener(() => OnButtonClicked(buttonIndexs[3].buttonIndex));
        mainButtons[4].onClick.AddListener(() => OnButtonClicked(buttonIndexs[4].buttonIndex));
        mainButtons[5].onClick.AddListener(() => OnButtonClicked(buttonIndexs[5].buttonIndex));
        mainButtons[6].onClick.AddListener(() => OnButtonClicked(buttonIndexs[6].buttonIndex));
        mainButtons[7].onClick.AddListener(() => OnButtonClicked(buttonIndexs[7].buttonIndex));
        mainButtons[8].onClick.AddListener(() => OnButtonClicked(buttonIndexs[8].buttonIndex));
    }

    private void OnButtonClicked(MainButtons whatButton)
    {
        switch(whatButton)
        {
            case MainButtons.BossFight:
                break;
            case MainButtons.close:
                CloseCurrentPannel();
                break;
            case MainButtons.Co_op:
                break;
            case MainButtons.Inventory:
                InventoryPannel.SetActive(true);
                currentOpenPannel = MainPannel.Inventory;
                break;
            case MainButtons.Option:
                break;
            case MainButtons.PvP:
                break;
            case MainButtons.Scenario:
                break;
            case MainButtons.Shop:
                break;
            case MainButtons.DebuggingItemAdd:
                GetItem();
                break;
        }
    }

    private void CloseCurrentPannel()
    {
        switch (currentOpenPannel)
        {
            case MainPannel.Inventory:
                InventoryPannel.SetActive(false);
                currentOpenPannel = MainPannel.Default;
                break;
            case MainPannel.Option:
                break;
            case MainPannel.Shop:
                break;
        }
    }

    /* Only Debugging
     * */
    private void GetItem()
    {
        Singleton<PlayerInfo>.Instance.GetItem(1, 1);
    }
}
