﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryScript : Singleton<InventoryScript> {
    const int AtLeastAmountOfSlots = 50;
    const int SlotsPerLow = 6;
    int amountOfSlots;
    int currentItemAmount;
    List<InventorySlotScript> slots = new List<InventorySlotScript>();

    [SerializeField]
    private GameObject AllertPannel;
    [SerializeField]
    private GameObject AllertCloseButton;
    

    public void InitIneventory(int amountOfSlots)
    {
        if(amountOfSlots < AtLeastAmountOfSlots)
        {
            this.amountOfSlots = AtLeastAmountOfSlots;
        }
        else
        {
            this.amountOfSlots = amountOfSlots;
        }

        Singleton<InventoryUImanager>.Instance.AddNewSlot(ref slots, this.amountOfSlots);
    }

    /* 아이템 획득
     * */
    public void AddItem(int itemNo, int itemLevel, int currentLevelExp, int currentGradeExp)
    {
        int emptySlot = SearchEmptySlot();
        ItemInfoStruct tempStruct = new ItemInfoStruct();
        Singleton<JSonManager>.Instance.GetUnitItemInfoStruct(ref tempStruct, itemNo.ToString(), itemLevel - 1);

        if(emptySlot == -1)
        {
            AllertPannel.SetActive(true);
            AllertCloseButton.SetActive(true);
        }
        else
        {
            slots[emptySlot].AddItem(tempStruct, currentLevelExp, currentGradeExp);
        }
    }

    /* 빈 슬롯이 있는지 검사하여 빈슬롯 반환
     * */
    public int SearchEmptySlot()
    {
        for(int index = 0; index < slots.Count; index++)
        {
            if(!slots[index].GetHasItem())
            {
                return index;
            }
        }
        return -1;
    }

    /* 슬롯 추가
     * */
    public void AddNewSlot(int amount)
    {
        Singleton<InventoryUImanager>.Instance.AddNewSlot(ref slots, amount);
    }

    /* 아이템 사용
     * */
    public bool UseItem(int slotIndex)
    {
        if (slots[slotIndex].GetHasItem())
        {
            slots[slotIndex].SetIsEquipped(false);
            return true;
        }

        return false;
    }

    /* 정렬
     * */
    public void SortInventorySlots()
    {
        
    }

    /* 현재 가진 아이템 개수 반환
     * */
    public int GetCurrentItemAmount()
    {
        return this.currentItemAmount;
    }

    /* 아이템 등급 업그레이드
     * */
    public void UpgradeItem(int slotIndex, int addExp)
    {
        slots[slotIndex].UpgradeItem(addExp);
    }

    /* 아이템 레벨 업그레이드
     * */
    public void LevelUpItem(int slotIndex, int addExp)
    {
        slots[slotIndex].LevelUpItem(addExp);
    }
}
