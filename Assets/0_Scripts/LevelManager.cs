﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 씬에 하나만 존재해야 하므로 싱글톤 클래스를 상속받도록 한다.
 * */
public class LevelManager : Singleton<LevelManager> {

    [SerializeField]
    private GameObject tilePrefab;

    private const int maxX = 18, maxY = 4;

    // 타일 정보를 저장하기 위한 딕셔너리
    public Dictionary<Vector2, TileScript> tileDictionary = new Dictionary<Vector2, TileScript>();
    // 네비게이션용 길을 관리하기 위한 딕셔너리
    public Dictionary<Vector2, Road> roadDictionary = new Dictionary<Vector2, Road>();
    // 타워 관리용 딕셔너리
    public Dictionary<Vector2, TowerScript> towerDictionary = new Dictionary<Vector2, TowerScript>();
    // 타워딕셔너리 인덱스 리스트
    public List<Vector2> towerDictionaryIndex = new List<Vector2>();
    // 오브젝트를 관리하기 위한 딕셔너리
    public Dictionary<int, ObjectInfo> objectInfoDictionary = new Dictionary<int, ObjectInfo>();
    // 오브젝트 딕셔너리 인덱스 리스트
    public List<int> objectDictionaryIndex = new List<int>();

	// Use this for initialization
	void Awake ()
    {
        SpawnTile();
	}

    /* 타일 소환.
     * 맵 에디터 만들 때 이 메소드를 기획자가 직접 컨트롤 할 수 있게 에디터에 노출 시키거나
     * 이 메소드를 대체할 수 있게 에디터 제작해야함.
     * 타일의 스테이트, 팀을 설정하고 tileDictionary에 새로 만든 타일을 추가.
     * */
    private void SpawnTile()
    {
        for(int y = - 1; y < maxY + 1; y++)
        {
            for(int x = - 1; x < maxX + 1; x++)
            {
                TileScript newTile = Instantiate(tilePrefab).GetComponent<TileScript>();
                newTile.gameObject.transform.position = new Vector3(x, y);

                if(x == -1 || x == maxX || y == -1 || y == maxY)
                {
                    newTile.init(TileState.Block, Team.Default);
                }

                else if(x < 2)
                {
                    newTile.init(TileState.Road, Team.ATeam);
                }
                else if(x < 6)
                {
                    newTile.init(TileState.CanBuildTower, Team.ATeam);
                }
                else if(x < 12 && (y == 0 || y == 3))
                {
                    newTile.init(TileState.Block, Team.Default);
                }
                else if(x < 12)
                {
                    newTile.init(TileState.Road, Team.Default);
                }
                else if(x < 16)
                {
                    newTile.init(TileState.CanBuildTower, Team.BTeam);
                }
                else
                {
                    newTile.init(TileState.Road, Team.BTeam);
                }
                tileDictionary.Add(new Vector2(x, y), newTile);
                SpawnRoad(newTile.transform.position, newTile.getState());
            }
        }

    }

    /* 유닛이 다니는 길 생성
     * Road 클래스를 만들어서 roadDictionary에 새로 만든 길을 추가.
     * */
    private void SpawnRoad(Vector2 tilePosition, TileState roadState)
    {
        for(int y = 0; y < 10; y++)
        {
            for(int x = -5; x < 5; x++)
            {
                Road newRoad = new Road(new Vector2(tilePosition.x * 10 + x, tilePosition.y * 10 + y), new Vector2(tilePosition.x + (x / 10.0f), tilePosition.y + (y / 10.0f)), roadState);
                roadDictionary.Add(newRoad.gridPosition, newRoad);
                //Debug.Log("로드 생성 : " + roadDictionary[new Vector2(tilePosition.x * 10 + x, tilePosition.y * 10 + y)].gridPosition + roadDictionary[new Vector2(tilePosition.x * 10 + x, tilePosition.y * 10 + y)].walkable);
            }
        }
    }

    /* AStar 알고리즘에서 유효한 좌표인지 확인할 때 사용
     * */
    public bool InBounds(Vector2 position)
    {
        return position.x >= 0 && position.y >= 0 && position.x < (maxX + 1) * 10 && position.y < (maxY + 1) * 10;
    }

    /* 건물 건설.
     * 
     * */
    public TowerScript BuildTower(int towerIndex, Vector2 buildPosition, Team team)
    {
        TowerScript tower = null;
        if (tileDictionary[buildPosition].state == TileState.CanBuildTower && tileDictionary[buildPosition].team == team)
        {
            tower = Singleton<ObjectPoolScript>.Instance.SpawnTowerObject(towerIndex, team).GetComponent<TowerScript>();
            tower.SetPosition(buildPosition);
            towerDictionary.Add(buildPosition, tower);
            towerDictionaryIndex.Add(buildPosition);
            tileDictionary[buildPosition].SetState(TileState.PlacedTower);
            if (team == Team.ATeam)
            {
                tileDictionary[buildPosition].ToggleTowerBuildCanvas();
            }
            UpdateRoad(buildPosition, TileState.PlacedTower);
        }

        return tower;
    }

    public void AddObjectInfoDic(ObjectInfo newObject)
    {
        int tempIndex = objectDictionaryIndex.Count;
        objectInfoDictionary.Add(tempIndex, newObject);
        objectDictionaryIndex.Add(tempIndex);
        newObject.objectCount = tempIndex;
    }

    public void SpawnUnitAtTurn(Team team)
    {
        for(int i = 0; i < towerDictionaryIndex.Count; i++)
        {
            if(towerDictionary[towerDictionaryIndex[i]].team == team)
            {
                towerDictionary[towerDictionaryIndex[i]].SpawnUnit();
            }
        }
    }

    public void UpdateRoad(Vector2 buildPosition, TileState state)
    {
        if (state == TileState.PlacedTower)
        {
            for(int y = 0; y < 10; y++)
            {
                for(int x = -5; x < 5; x++)
                {
                    if(x == -5 || x == 4 || y == 0 || y == 9)
                    {
                        roadDictionary[new Vector2(buildPosition.x * 10 + x, buildPosition.y * 10 + y)].SetState(TileState.Road);
                    }
                    else
                    {
                        roadDictionary[new Vector2(buildPosition.x * 10 + x, buildPosition.y * 10 + y)].SetState(TileState.PlacedTower);
                    }
                }
            }
        }
        else if (state == TileState.CanBuildTower)
        {
            for(int y = 0; y < 10; y ++)
            {
                for(int x = 0; x < 10; x++)
                {
                    roadDictionary[new Vector2(buildPosition.x * 10 + x, buildPosition.y * 10 + y)].SetState(TileState.Road);
                }
            }
        }
    }

    public void UpdateTileState(Vector2 buildPosition, TileState state)
    {
        tileDictionary[buildPosition].SetState(state);
        UpdateRoad(buildPosition, state);
    }
}
