﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitScript : MonoBehaviour {

    private Rigidbody2D myRB2D;
    private SpriteRenderer mySpriteRenderer;

    [SerializeField]
    private RangeScript attackRader;
    [SerializeField]
    private RangeScript sightRader;
    [SerializeField]
    private bool hasProjectile;
    [SerializeField]
    private GameObject projectile;
    private ObjectInfo myObjectInfo;
    [SerializeField]
    private GameObject splashZonePrefab;
    private GameObject mySplashZone = null;
    [SerializeField]
    private Slider healthBar;

    private Team team;
    private UnitState state;
    [SerializeField]
    private string unitKey;
    private UnitName unitName;
    private UnitInfoStruct initInfo;
    private UnitInfoStruct currentInfo;
    private bool buffed;
    public int[] objectPoolIndex { get; private set; }

    private Stack<Node> finalPath;
    private Vector2 gridPosition;
    private Vector2 destination;
    private Vector2 lastDestination;
    private ObjectInfo closedTarget;
    private List<ObjectInfo> closedMultipleTarget = new List<ObjectInfo>();
    private float countUpdatePathTime;
    private float countUpdateAttackTime;
    private float countUpdateStateTime;
    private const float updatePathTime = 0.5f;
    private const float updateStateTime = 0.1f;

    // Use this for initialization
    void Awake ()
    {
        myRB2D = GetComponent<Rigidbody2D>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        myObjectInfo = GetComponent<ObjectInfo>();

        
	}
	
	// Update is called once per frame
	void Update ()
    {
        mySpriteRenderer.sortingOrder = (int)((18 - transform.position.x) * 100) + (int)((4 - transform.position.y) * 100);

        if(closedTarget != null && currentInfo.hasSplashZone)
        {
            mySplashZone.transform.position = Vector2.MoveTowards(mySplashZone.transform.position, closedTarget.transform.position, 8 * Time.deltaTime);
        }
        else if(closedTarget == null && currentInfo.hasSplashZone)
        {
            mySplashZone.transform.position = Vector2.MoveTowards(mySplashZone.transform.position, transform.position, 8 * Time.deltaTime);
        }

        if (countUpdateStateTime >= updateStateTime)
        {
            state = CheckState();
            countUpdateStateTime = 0;
        }

        if (state == UnitState.Attack && countUpdateAttackTime >= currentInfo.attackSpeed)
        { 
                Attack();
                countUpdateAttackTime = 0;
        }

        else if(state == UnitState.Move)
        {
            if(countUpdatePathTime > updatePathTime)
            {
                gridPosition = GetGridPosition(transform.position);
                GeneratePath();
                countUpdatePathTime = 0;
            }
            Move();
        }

        countUpdatePathTime += Time.deltaTime;
        countUpdateAttackTime += Time.deltaTime;
        countUpdateStateTime += Time.deltaTime;
	}

    /* 오브젝트 풀에 생성시에 실행하는 초기화 메소드
     * */
    public void Init(Team team, int[] objectPoolIndex)
    {
        this.team = team;
        this.objectPoolIndex = objectPoolIndex;
        myObjectInfo.team = team;
    }

    /* 인게임에서 유닛 소환시 실행되는 초기화 메소드
     * */
    public void InitSpawned(int level, Vector2 position)
    {
        transform.position = position;
        Singleton<JSonManager>.Instance.GetUnitInfoStruct(ref initInfo, unitKey, level);
        currentInfo = initInfo;
        UpdateHealthBar();
        closedTarget = null;

        if (mySplashZone == null && currentInfo.hasSplashZone)
        {
            mySplashZone = Instantiate(splashZonePrefab);
            mySplashZone.SetActive(true);
        }
        else if (currentInfo.hasSplashZone)
        {
            mySplashZone.SetActive(true);
        }

        if (currentInfo.attackType == AttackType.단일힐 || currentInfo.attackType == AttackType.범위힐)
        {
            if (mySplashZone != null)
            {
                sightRader.InitRange(team, currentInfo.sightRange, true, mySplashZone.GetComponent<SplashAttackScript>());
                attackRader.InitRange(team, currentInfo.attackRange, true, mySplashZone.GetComponent<SplashAttackScript>());
            }
            else
            {
                sightRader.InitRange(team, currentInfo.sightRange, true, null);
                attackRader.InitRange(team, currentInfo.attackRange, true, null);
            }
        }
        else
        {
            if(team == Team.ATeam)
            {
                if (mySplashZone != null)
                {
                    sightRader.InitRange(Team.BTeam, currentInfo.sightRange, false, mySplashZone.GetComponent<SplashAttackScript>());
                    attackRader.InitRange(Team.BTeam, currentInfo.attackRange, false, mySplashZone.GetComponent<SplashAttackScript>());
                }
                else
                {
                    sightRader.InitRange(Team.BTeam, currentInfo.sightRange, false, null);
                    attackRader.InitRange(Team.BTeam, currentInfo.attackRange, false, null);
                }
            }
            else
            {
                if (mySplashZone != null)
                {
                    sightRader.InitRange(Team.ATeam, currentInfo.sightRange, false, mySplashZone.GetComponent<SplashAttackScript>());
                    attackRader.InitRange(Team.ATeam, currentInfo.attackRange, false, mySplashZone.GetComponent<SplashAttackScript>());
                }
                else
                {
                    sightRader.InitRange(Team.ATeam, currentInfo.sightRange, false, null);
                    attackRader.InitRange(Team.ATeam, currentInfo.attackRange, false, null);
                }
            }
        }
        
        gridPosition = GetGridPosition(transform.position);
        myObjectInfo.gridPosition = gridPosition;
        if (team == Team.ATeam)
        {
            lastDestination = new Vector2(160, (int)position.y * 10);
        }
        else if(team == Team.BTeam)
        {
            lastDestination = new Vector2(0, (int)position.y * 10);
        }
        GeneratePath();
    }

    private void GeneratePath()
    {
        if (currentInfo.attackType == AttackType.단일힐 || currentInfo.attackType == AttackType.범위힐)
        {
            closedTarget = sightRader.FindHealingTarget(currentInfo.attackPoint);
        }
        else
        {
            closedTarget = sightRader.FindClosedAttackTarget();
        }

        Vector2 enemyGridPosition;

        if (closedTarget != null)
        {
            if (closedTarget.isUnit)
            {
                enemyGridPosition = GetGridPosition(closedTarget.transform.position);
            }
            else
            {
                enemyGridPosition = GetGridPosition(new Vector2(closedTarget.transform.position.x, closedTarget.transform.position.y - 0.1f));
            }
            finalPath = Astar.GetPath(gridPosition, enemyGridPosition);
            if (finalPath != null && gridPosition != enemyGridPosition && finalPath.Count > 0)
            {
                destination = finalPath.Pop().roadRef.worldPosition;
            }
        }
        else if (gridPosition != lastDestination)
        {
            finalPath = Astar.GetPath(gridPosition, lastDestination);
            if (finalPath != null && gridPosition != lastDestination)
            {
                destination = finalPath.Pop().roadRef.worldPosition;
            }
        }
    }

    private Vector2 GetGridPosition(Vector2 worldPosition)
    {
        return new Vector2((int)(worldPosition.x * 10), (int)(worldPosition.y * 10));
    }

    private void SetPath(Stack<Node> newPath)
    {
        if (newPath != null)
        {
            this.finalPath = newPath;

            gridPosition = newPath.Peek().GridPosition;
            destination = newPath.Pop().roadRef.worldPosition;
        }
    }

    private void Move()
    {
        transform.position = Vector2.MoveTowards(transform.position, destination, currentInfo.moveSpeed * Time.deltaTime);
        if ((Vector2)transform.position == destination && finalPath.Count > 0 && finalPath != null)
        {
            gridPosition = finalPath.Peek().GridPosition;
            destination = finalPath.Pop().roadRef.worldPosition;
        }
    }

    public float GetDamagedPoint()
    {
        return currentInfo.maxHealthPoint - currentInfo.healthPoint;
    }

    public UnitType GetUnitType()
    {
        return currentInfo.unitType;
    }

    private UnitState CheckState()
    {
        if (currentInfo.attackType == AttackType.단일힐 || currentInfo.attackType == AttackType.범위힐)
        {
            if(attackRader.targetsIndex.Count > 0)
            {
                closedTarget = attackRader.FindHealingTarget(currentInfo.attackPoint);
                if(closedTarget != null)
                {
                    return UnitState.Attack;
                }
            }
            else
            {
                closedTarget = sightRader.FindHealingTarget(currentInfo.attackPoint);
            }
        }
        else
        {
            attackRader.TargetCheck();
        }
        
        if(attackRader.targetsIndex.Count > 0 && currentInfo.unitType != UnitType.사제)
        {
            return UnitState.Attack;
        }

        return UnitState.Move;
    }

    /* state가 Attack일 때 실행.
     * 공격 유형, 스킬에 따라서 알맞은 형태의 공격 실행.
     * */
    private void Attack()
    {
        if (closedTarget == null)
        {
            if (GetIsHealer())
            {
                closedTarget = attackRader.FindHealingTarget(currentInfo.attackPoint);
            }
            else
            {
                closedTarget = attackRader.FindClosedAttackTarget();
            }
            if(closedTarget == null)
            {
                return;
            }
        }
        if (closedTarget.gameObject.activeInHierarchy == false)
        {
            if (GetIsHealer())
            {
                closedTarget = attackRader.FindHealingTarget(currentInfo.attackPoint);
            }
            else
            {
                closedTarget = attackRader.FindClosedAttackTarget();
            }
        }
        switch(currentInfo.attackType)
        {
            case AttackType.근접단일 :
                attackRader.ShortDistanceSingleAttack(closedTarget, currentInfo.attackPoint);
                break;
            case AttackType.근접범위 :
                attackRader.ShortDistanceSplashAttack(currentInfo.attackPoint);
                break;
            case AttackType.단일힐 :
                attackRader.LongDistanceSingleImmediatelyAttack(closedTarget, -currentInfo.attackPoint);
                break;
            case AttackType.발사체다중 :
                ProjectileMultipleAttack();
                break;
            case AttackType.발사체단일 :
                ProjectileSingleAttack();
                break;
            case AttackType.발사체범위 :
                ProjectileSplashAttack();
                break;
            case AttackType.발사체쿠션:
                ProjectileCushionAttack();
                break;
            case AttackType.범위힐:
                attackRader.LongDistanceSplashImmediatelyAttack(-currentInfo.attackPoint);
                break;
            case AttackType.원거리범위지속:
                break;
            case AttackType.원거리즉시단일:
                attackRader.LongDistanceSingleImmediatelyAttack(closedTarget, currentInfo.attackPoint);
                break;
            case AttackType.원거리즉시범위:
                attackRader.LongDistanceSplashImmediatelyAttack(currentInfo.attackPoint);
                break;
        }
    }

    /* 공격을 받아 데미지를 입을 때 실행.
     * 체력이 0 이하가 되면 사망 처리.
     * */
    public void GetDamage(float damagePoint)
    {
        if (damagePoint - currentInfo.defensePoint >= 1 || damagePoint < 0)
        {
            currentInfo.healthPoint -= damagePoint - currentInfo.defensePoint;
        }
        else
        {
            currentInfo.healthPoint -= 1;
        }

        UpdateHealthBar();

        if(currentInfo.healthPoint <= 0)
        {
            Singleton<ObjectPoolScript>.Instance.DeSpawnUnitObject(objectPoolIndex, team);
            attackRader.ClearTargets();
            sightRader.ClearTargets();
            gameObject.SetActive(false);
            currentInfo.healthPoint = 1000;
        }
    }

    public GameObject GetProjectileObject()
    {
        return projectile;
    }

    public bool GetHasProjectile()
    {
        return hasProjectile;
    }

    public bool GetIsHealer()
    {
        if(currentInfo.attackType == AttackType.단일힐 || currentInfo.attackType == AttackType.범위힐)
        {
            return true;
        }

        return false;
    }

    private void ProjectileSingleAttack()
    {
        GameObject tempProjectile = Singleton<ObjectPoolScript>.Instance.SpawnProjectileObject(objectPoolIndex[0], team);
        tempProjectile.GetComponent<ProjectileScript>().InitSpawned(currentInfo.attackPoint, 5, closedTarget, false, transform.position);
    }

    private void ProjectileMultipleAttack()
    {
        attackRader.FindClosedMultipleAttackTarget(3, ref closedMultipleTarget);
        GameObject tempProjectile = null;
        for(int i = 0; i < closedMultipleTarget.Count; i++)
        {
            tempProjectile = Singleton<ObjectPoolScript>.Instance.SpawnProjectileObject(objectPoolIndex[0], team);
            tempProjectile.GetComponent<ProjectileScript>().InitSpawned(currentInfo.attackPoint, 5, closedMultipleTarget[i], false, transform.position);
        }
    }

    private void ProjectileSplashAttack()
    {
        GameObject tempProjectile = Singleton<ObjectPoolScript>.Instance.SpawnProjectileObject(objectPoolIndex[0], team);
        tempProjectile.GetComponent<ProjectileScript>().InitSpawned(currentInfo.attackPoint, 5, closedTarget, true, transform.position);
    }

    private void ProjectileCushionAttack()
    {
        
    }

    private void UpdateHealthBar()
    {
        healthBar.value = currentInfo.healthPoint / currentInfo.maxHealthPoint;
    }
}
