﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TileScript : MonoBehaviour {
    public TileState state { get; private set; }
    public Team team { get; private set; }

    [SerializeField]
    // 타워 건설 버튼이 있는 캔버스
    private GameObject towerBuildCanvas;
    // 타워 건설 버튼
    [SerializeField]
    private Button[] towerBuildButton;
    [SerializeField]
    private Button closeTowerBuildButton;
    private List<BuildButtonScript> towerBuildButtonScripts = new List<BuildButtonScript>();
    [SerializeField]
    private GameObject block;

    SpriteRenderer mySpriteRenderer;

    // Use this for initialization
    void Awake()
    {
        mySpriteRenderer = GetComponent<SpriteRenderer>();

        towerBuildButtonScripts.Add(towerBuildButton[0].GetComponent<BuildButtonScript>());
        towerBuildButton[0].onClick.AddListener(() => OnClickedTowerBuildButton(towerBuildButtonScripts[0].buttonIndex));
        towerBuildButtonScripts.Add(towerBuildButton[1].GetComponent<BuildButtonScript>());
        towerBuildButton[1].onClick.AddListener(() => OnClickedTowerBuildButton(towerBuildButtonScripts[1].buttonIndex));
        towerBuildButtonScripts.Add(towerBuildButton[2].GetComponent<BuildButtonScript>());
        towerBuildButton[2].onClick.AddListener(() => OnClickedTowerBuildButton(towerBuildButtonScripts[2].buttonIndex));
        towerBuildButtonScripts.Add(towerBuildButton[3].GetComponent<BuildButtonScript>());
        towerBuildButton[3].onClick.AddListener(() => OnClickedTowerBuildButton(towerBuildButtonScripts[3].buttonIndex));
        towerBuildButtonScripts.Add(towerBuildButton[4].GetComponent<BuildButtonScript>());
        towerBuildButton[4].onClick.AddListener(() => OnClickedTowerBuildButton(towerBuildButtonScripts[4].buttonIndex));
        towerBuildButtonScripts.Add(towerBuildButton[5].GetComponent<BuildButtonScript>());
        towerBuildButton[5].onClick.AddListener(() => OnClickedTowerBuildButton(towerBuildButtonScripts[5].buttonIndex));
        closeTowerBuildButton.onClick.AddListener(OnClickedCloseTowerBuildButton);       
    }
	
	// Update is called once per frame
	void Update () {
        mySpriteRenderer.sortingOrder = -(int)(transform.position.y + transform.position.x);
    }

    /* 초기화
     * */
    public void init(TileState state, Team team)
    {
        this.state = state;
        this.team = team;
        if(this.state == TileState.Block)
        {
            block.SetActive(true);
        }
    }

    public TileState getState()
    {
        return state;
    }

    public void SetState(TileState newState)
    {
        this.state = newState;
    }

    public void ToggleTowerBuildCanvas()
    {
        towerBuildCanvas.SetActive(!towerBuildCanvas.active);
    }

    private void OnClickedTowerBuildButton(int buttonIndex)
    {
        Singleton<PlayerController>.Instance.BuildTower(buttonIndex, transform.position);
    }

    private void OnClickedCloseTowerBuildButton()
    {
        Singleton<PlayerController>.Instance.CloseBuildTowerButton();
    }
}
