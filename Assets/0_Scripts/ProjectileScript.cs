﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour
{
    private float moveSpeed;
    private float attackPoint;
    private bool isSplashAttack;
    private int targetObjectCount;
    private ObjectInfo targetObjectInfo;
    private Team targetTeam;
    private Team myTeam;
    public int[] objectPoolIndex { get; private set; }

    private int cushionCount;

    [SerializeField]
    private SplashAttackScript splashZone;
    

    // Update is called once per frame
    void Update()
    {
        if (targetObjectInfo.gameObject.activeInHierarchy)
        {
            transform.position = Vector2.MoveTowards(transform.position, targetObjectInfo.transform.position, moveSpeed * Time.deltaTime);
        }
        else
        {
            gameObject.SetActive(false);
            Singleton<ObjectPoolScript>.Instance.DeSpawnProjectileObject(objectPoolIndex, myTeam);
        }
    }

    public void Init(Team targetTeam, Team myTeam, int[] objectPoolIndex)
    {
        this.targetTeam = targetTeam;
        this.myTeam = myTeam;
        this.objectPoolIndex = objectPoolIndex;
    }

    public void InitSpawned(float attackPoint, float moveSpeed, ObjectInfo targetObjectInfo, bool isSplashAttack, Vector2 spawnPosition)
    {
        this.attackPoint = attackPoint;
        this.moveSpeed = moveSpeed;
        if(targetObjectInfo == null)
        {
            gameObject.SetActive(false);
            Singleton<ObjectPoolScript>.Instance.DeSpawnProjectileObject(objectPoolIndex, myTeam);
        }
        this.targetObjectCount = targetObjectInfo.objectCount;
        this.targetObjectInfo = targetObjectInfo;
        this.isSplashAttack = isSplashAttack;
        transform.position = spawnPosition;

        if(this.isSplashAttack)
        {
            splashZone.gameObject.SetActive(true);
            splashZone.SetTargetTeam(targetTeam);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other != null)
        {
            ObjectInfo tempTarget = other.transform.parent.GetComponent<ObjectInfo>();

            if(tempTarget.objectCount == targetObjectCount)
            {
                if(isSplashAttack)
                {
                    for(int i = 0; i < splashZone.targetsObjectInfo.Count; i++)
                    {
                        splashZone.targetsObjectInfo[splashZone.targetsIndex[i]].GetDamage(attackPoint);
                    }
                    gameObject.SetActive(false);
                    splashZone.DictionaryClear();
                    Singleton<ObjectPoolScript>.Instance.DeSpawnProjectileObject(objectPoolIndex, myTeam);
                }
                else
                {
                    tempTarget.GetDamage(attackPoint);
                    gameObject.SetActive(false);
                    Singleton<ObjectPoolScript>.Instance.DeSpawnProjectileObject(objectPoolIndex, myTeam);
                }
            }
        }
    }

    public Team GetTargetTeam()
    {
        return targetTeam;
    }
}
