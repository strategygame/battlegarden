﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using System.IO;

public class JSonManager : Singleton<JSonManager>
{
    private string unitInfoJSonAdress;
    private string towerInfoJSonAdress;
    private string unitItemInfoJSonAdress;

    private JsonData unitInfoJSonData;
    private JsonData towerInfoJSonData;
    private JsonData unitItemInfoJSonData;

    void Awake()
    {
        unitInfoJSonAdress = Application.dataPath + "/1_Resources/1_JSon/unitInfo.json";
        towerInfoJSonAdress = Application.dataPath + "/1_Resources/1_JSon/towerInfo.json";
        unitItemInfoJSonAdress = Application.dataPath + "/1_Resources/1_JSon/itemInfo.json";

        LoadTowerInfoStructForJSon();
        LoadUnitInfoStructForJSon();
        LoadUnitItemInfoStructForJSon();
    }

    private void LoadUnitInfoStructForJSon()
    {
        string jSonString = File.ReadAllText(unitInfoJSonAdress);

        unitInfoJSonData = JsonMapper.ToObject(jSonString);
    }

    private void LoadTowerInfoStructForJSon()
    {
        string jSonString = File.ReadAllText(towerInfoJSonAdress);

        towerInfoJSonData = JsonMapper.ToObject(jSonString);
    }

    private void LoadUnitItemInfoStructForJSon()
    {
        string jSonString = File.ReadAllText(unitItemInfoJSonAdress);

        unitItemInfoJSonData = JsonMapper.ToObject(jSonString);
    }

    public void GetUnitInfoStruct(ref UnitInfoStruct unitInfo, string key, int level)
    {
        unitInfo.attackPoint = Convert.ToSingle(unitInfoJSonData[key][level]["attackPoint"].ToString());
        unitInfo.attackPriority = (AttackPriority)Enum.Parse(typeof(AttackPriority), unitInfoJSonData[key][level]["attackPriority"].ToString());
        unitInfo.attackRange = Convert.ToSingle(unitInfoJSonData[key][level]["attackRange"].ToString());
        unitInfo.attackSpeed = Convert.ToSingle(unitInfoJSonData[key][level]["attackSpeed"].ToString());
        unitInfo.attackType = (AttackType)Enum.Parse(typeof(AttackType), unitInfoJSonData[key][level]["attackType"].ToString());
        unitInfo.buffScale = Convert.ToSingle(unitInfoJSonData[key][level]["buffScale"].ToString());
        unitInfo.canBuff = Convert.ToBoolean(unitInfoJSonData[key][level]["canBuff"].ToString());
        unitInfo.defensePoint = Convert.ToSingle(unitInfoJSonData[key][level]["defensePoint"].ToString());
        unitInfo.hasSplashZone = Convert.ToBoolean(unitInfoJSonData[key][level]["hasSplashZone"].ToString());
        unitInfo.healthPoint = Convert.ToSingle(unitInfoJSonData[key][level]["maxHealthPoint"].ToString());
        unitInfo.level = Convert.ToInt32(unitInfoJSonData[key][level]["level"].ToString());
        unitInfo.maxHealthPoint = Convert.ToSingle(unitInfoJSonData[key][level]["maxHealthPoint"].ToString());
        unitInfo.moveSpeed = Convert.ToSingle(unitInfoJSonData[key][level]["moveSpeed"].ToString());
        unitInfo.sightRange = 5;
        unitInfo.unitType = (UnitType)Enum.Parse(typeof(UnitType), unitInfoJSonData[key][level]["unitType"].ToString());
    }

    public void GetTowerInfoStruct(ref TowerInfoStruct towerInfo, string key, int level)
    {
        Debug.Log(towerInfoJSonData[key][level]["name"]);
        towerInfo.attackPoint = Convert.ToSingle(towerInfoJSonData[key][level]["attackPoint"].ToString());
        towerInfo.attackRange = Convert.ToSingle(towerInfoJSonData[key][level]["attackRange"].ToString());
        towerInfo.attackSpeed = Convert.ToSingle(towerInfoJSonData[key][level]["attackSpeed"].ToString());
        towerInfo.attackType = (AttackType)Enum.Parse(typeof(AttackType), towerInfoJSonData[key][level]["attackType"].ToString());
        towerInfo.buildCost = Convert.ToInt32(towerInfoJSonData[key][level]["buildCost"].ToString());
        towerInfo.canAttack = Convert.ToBoolean(towerInfoJSonData[key][level]["canAttack"].ToString());
        towerInfo.defensePoint = Convert.ToSingle(towerInfoJSonData[key][level]["defensePoint"].ToString());
        towerInfo.healthPoint = Convert.ToSingle(towerInfoJSonData[key][level]["maxHealthPoint"].ToString());
        towerInfo.level = Convert.ToInt32(towerInfoJSonData[key][level]["level"].ToString());
        towerInfo.levelUpCost = Convert.ToInt32(towerInfoJSonData[key][level]["levelUpCost"].ToString());
        towerInfo.maintenanceCost = Convert.ToInt32(towerInfoJSonData[key][level]["maintenanceCost"].ToString());
        towerInfo.maxHealthPoint = Convert.ToSingle(towerInfoJSonData[key][level]["maxHealthPoint"].ToString());
        towerInfo.spawnUnitAmount = Convert.ToInt32(towerInfoJSonData[key][level]["spawnUnitAmount"].ToString());
    }

    public void GetUnitItemInfoStruct(ref ItemInfoStruct itemInfo, string key, int level)
    {
        Debug.Log(unitItemInfoJSonData[key][level]);
        //itemInfo.itemNo = Convert.ToInt32(unitItemInfoJSonData[key][level]["itemNo"].ToString());
        itemInfo.name = unitItemInfoJSonData[key][level]["name"].ToString();
        itemInfo.description = unitItemInfoJSonData[key][level]["description"].ToString();
        itemInfo.towerID = unitItemInfoJSonData[key][level]["towerID"].ToString();
        itemInfo.level = Convert.ToInt32(unitItemInfoJSonData[key][level]["level"].ToString());
        itemInfo.maxLevelExp = Convert.ToInt32(unitItemInfoJSonData[key][level]["maxLevelExp"].ToString());
        itemInfo.grade = Convert.ToInt32(unitItemInfoJSonData[key][level]["grade"].ToString());
        itemInfo.maxGradeExp = Convert.ToInt32(unitItemInfoJSonData[key][level]["maxGradeExp"].ToString());
        itemInfo.provideLevelExp = Convert.ToInt32(unitItemInfoJSonData[key][level]["provideLevelExp"].ToString());
        itemInfo.provideGradeExp = Convert.ToInt32(unitItemInfoJSonData[key][level]["provideGradeExp"].ToString());
    }
       
    /* 플레이어 정보 불러오기
     * */
    public void GetPlayerInfo()
    {
        
    }

    /* 플레이어 정보 업데이트
     * */
    public void UpdatePlayerInfo()
    {

    }

    /* 새 플레이어 등록
     * */
    public void SaveNewPlayerInfo()
    {

    }
}
