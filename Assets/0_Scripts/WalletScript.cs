﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalletScript : MonoBehaviour {
    public int money { get; private set; }
    public int income { get; private set; }

    public void Init(Team team)
    {
        if(team == Team.ATeam)
        {
            money = 100;
            income = 100;
        }
        else if(team == Team.BTeam)
        {
            money = 130;
            income = 100;
        }
    }

    public void AddMoney(int add)
    {
        this.money += add;
    }

    public void AddIncome(int add)
    {
        this.income += add;
    }

    public void GetIncome()
    {
        this.money += this.income;
    }
}
