﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TileState
{
    Default,
    CanBuildTower,
    PlacedTower,
    Road,
    Block,
    OnUnit,
}

public enum Team
{
    Default,
    ATeam,
    BTeam
}

public enum AttackPriority
{
    Default,
    근접,
    법사,
    궁수,
    힐러,
    버퍼,
    타워,
    영웅
}

public enum AttackType
{
    Default,
    근접단일,
    근접범위,
    발사체단일,
    발사체범위,
    발사체다중,
    발사체쿠션,
    원거리즉시단일,
    원거리즉시범위,
    원거리범위지속,
    단일힐,
    범위힐
}

public enum UnitName
{
    알렉스,
    멀린,
    우르바노,
    크루스,
    로빈,
    페트루,
    무르,
    파티마,
    흑마,
    가고일,
    나이트,
    바아라크,
    소환수,
    단탈리온,
    서큐버스,
    라볼라스,
    푸르푸르
}

public enum UnitType
{
    Default,
    전사,
    궁수,
    마법사,
    사제,
    기사,
    도적,
    흑마법사,
    소환사
}

public enum UnitState
{
    Idle,
    Move,
    Attack
}

public enum TowerType
{
    Default,
    유닛생산,
    방어타워,
    넥서스
}

public enum Turn
{
    Default,
    ATeam,
    BTeam
}

public enum AIState
{
    Default,
    BuildTower,
    UpgradeTower,
    UpgradeNexus
}

public enum MainButtons
{
    Default,
    Scenario,
    PvP,
    Co_op,
    BossFight,
    Inventory,
    Shop,
    Option,
    close,
    DebuggingItemAdd
}

public enum MainPannel
{
    Default,
    Inventory,
    Shop,
    Option
}

public enum SortType
{
    Grade,
    Level,
    Class
}

public enum PvPGrade
{
    a,
    b,
    c,
    d,
    e,
    f
}