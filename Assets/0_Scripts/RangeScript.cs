﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeScript : MonoBehaviour
{

    private Team targetTeam;
    private float maxDistanceToTarget;
    private float CurrentDistanceToTarget;
    private bool isHealer;
    private AttackPriority attackPriority;
    public Dictionary<int, GameObject> targets = new Dictionary<int, GameObject>();
    public Dictionary<int, ObjectInfo> targetsObjectInfo = new Dictionary<int, ObjectInfo>();
    public List<int> targetsIndex = new List<int>();
    
    private SplashAttackScript splashZone = null;

    public void InitRange(Team targetTeam, float Range, bool isHealer, SplashAttackScript splashZone)
    {
        this.targetTeam = targetTeam;
        this.maxDistanceToTarget = Range;
        this.CurrentDistanceToTarget = maxDistanceToTarget;
        GetComponent<CircleCollider2D>().radius = Range;
        this.isHealer = isHealer;
        this.splashZone = splashZone;
        if (this.splashZone != null)
        {
            this.splashZone.SetTargetTeam(this.targetTeam);
        }
    }

    /* 콜라이더(감지 범위)안에 타게팅 대상이 들어왔을 때.
     * 타겟이 올바른지 확인 후 딕셔너리에 추가.
     * */
    private void OnTriggerEnter2D(Collider2D other)
    {
        ObjectInfo newTarget = other.transform.parent.GetComponent<ObjectInfo>();
        if (!isHealer && newTarget.team == targetTeam)
        {
            targetsObjectInfo.Add(newTarget.objectCount, newTarget);
            targets.Add(newTarget.objectCount, other.gameObject);
            targetsIndex.Add(newTarget.objectCount);
        }
        else if (isHealer && newTarget.isUnit && newTarget.team == targetTeam)
        {
            targetsObjectInfo.Add(newTarget.objectCount, newTarget);
            targets.Add(newTarget.objectCount, other.gameObject);
            targetsIndex.Add(newTarget.objectCount);
        }
    }

    /* 콜라이더(감지 범위)안의 타게팅 대상이 밖으로 벗어났을 때.
     * 딕셔너리에 포함된 타겟인지 확인 후 딕셔너리에서 제거.
     * */
    private void OnTriggerExit2D(Collider2D other)
    {
        ObjectInfo outTarget = other.transform.parent.GetComponent<ObjectInfo>();
        if (!isHealer && outTarget.team == targetTeam)
        {
            targetsObjectInfo.Remove(outTarget.objectCount);
            targets.Remove(outTarget.objectCount);
            targetsIndex.Remove(outTarget.objectCount);
        }
        if(isHealer && outTarget.isUnit && outTarget.team == targetTeam)
        {
            targetsObjectInfo.Remove(outTarget.objectCount);
            targets.Remove(outTarget.objectCount);
            targetsIndex.Remove(outTarget.objectCount);
        }
    }

    /* 가까운 적을 확인하여 반환.
     * */
    public ObjectInfo FindClosedAttackTarget()
    {
        ObjectInfo closestTarget = null;
        ObjectInfo tempTarget = null;
        if (targets.Count > 0 && targetsIndex.Count > 0)
        {
            for (int i = 0; i < targetsIndex.Count; i++)
            {
                tempTarget = targetsObjectInfo[targetsIndex[i]];
                if (tempTarget != null)
                {
                    if (closestTarget == null)
                    {
                        closestTarget = tempTarget;
                    }
                    else if (Mathf.Pow(transform.position.x - tempTarget.transform.position.x, 2) + Mathf.Pow(transform.position.y - tempTarget.transform.position.y, 2) < CurrentDistanceToTarget)
                    {
                        closestTarget = tempTarget;
                        CurrentDistanceToTarget = Mathf.Pow(transform.position.x - tempTarget.transform.position.x, 2) + Mathf.Pow(transform.position.y - tempTarget.transform.position.y, 2);
                    }
                }
            }
        }
        CurrentDistanceToTarget = maxDistanceToTarget;
        return closestTarget;
    }

    /* 가까운 적을 count만큼 리스트에 저장하여 반환.
     * */
    public void FindClosedMultipleAttackTarget(int count, ref List<ObjectInfo> closedMultipleTarget)
    {
        closedMultipleTarget.Clear();
        for (int i = 0; i < count; i++)
        {
            if (targetsObjectInfo.Count < i + 1)
            {
                break;
            }
            closedMultipleTarget.Add(targetsObjectInfo[targetsIndex[i]]);
        }
    }

    /* 힐링 대상을 확인하여 반환.
     * 회복시킬 수 있는 최대치에 가깝게 데미지를 입은 유닛을 우선하여 타게팅.
     * */
    public ObjectInfo FindHealingTarget(float healingPoint)
    {
        ObjectInfo findTarget = null;
        ObjectInfo tempTarget = null;
        float needHealingPoint = 0;

        if (targets.Count > 0)
        {
            for(int i = 0; i < targetsIndex.Count; i++)
            {
                tempTarget = targetsObjectInfo[targetsIndex[i]];
                if (tempTarget != null)
                {
                    if (tempTarget.GetDamagedPoint() > needHealingPoint)
                    {
                        findTarget = tempTarget;
                        needHealingPoint = tempTarget.GetDamagedPoint();
                        if (needHealingPoint > healingPoint)
                        {
                            break;
                        }
                    }
                }
            }
        }

        return findTarget;
    }

    /* 공격 우선순위 체크.
     * 기획에 맞게 수정 필요.
     * */
    private bool AttackPriorityCheck(AttackPriority priority, GameObject target)
    {
        if (priority == AttackPriority.Default)
        {
            return true;
        }
        if (!target.GetComponent<ObjectInfo>().isUnit && priority == AttackPriority.타워)
        {
            return true;
        }
        else
        {
            if (priority == AttackPriority.근접 && target.GetComponent<UnitScript>().GetUnitType() == UnitType.전사)
            {
                return true;
            }
            if (priority == AttackPriority.법사 && target.AddComponent<UnitScript>().GetUnitType() == UnitType.마법사)
            {
                return true;
            }
            if (priority == AttackPriority.궁수 && target.GetComponent<UnitScript>().GetUnitType() == UnitType.궁수)
            {
                return true;
            }
            if (priority == AttackPriority.힐러 && target.AddComponent<UnitScript>().GetUnitType() == UnitType.사제)
            {
                return true;
            }
            if (priority == AttackPriority.버퍼 && target.AddComponent<UnitScript>().GetUnitType() == UnitType.기사)
            {
                return true;
            }
        }
        return false;
    }

    public void HealingTargetCheck()
    {
        for(int i = 0; i < targetsIndex.Count; i++)
        {
            if(targetsObjectInfo[targetsIndex[i]].GetDamagedPoint() <= 0)
            {
                targets.Remove(targetsIndex[i]);
                targetsIndex.RemoveAt(i);
            }
        }
    }

    /* 근거리 단일 공격
     * */
    public void ShortDistanceSingleAttack(ObjectInfo target, float attackPoint)
    {
        if(target != null)
        {
            target.GetDamage(attackPoint);
        }
    }

    /* 근거리 범위 공격
     * 캐릭터 주변 반경 collider.Radius 안의 모든 타겟에 적용.
     * */
    public void ShortDistanceSplashAttack(float attackPoint)
    {
        for(int i = 0; i < targetsIndex.Count; i++)
        {
            if(targets[targetsIndex[i]] != null)
            {
                if(targets[targetsIndex[i]].CompareTag("Building"))
                {
                    targets[targetsIndex[i]].GetComponent<TowerScript>().GetDamage(attackPoint);
                }
                else if(targets[targetsIndex[i]].CompareTag("Unit"))
                {
                    targets[targetsIndex[i]].GetComponent<UnitScript>().GetDamage(attackPoint);
                }
            }
        }
    }

    /* 원거리 단일 즉시 공격
     * */
    public void LongDistanceSingleImmediatelyAttack(ObjectInfo target, float attackPoint)
    {
        if (target != null)
        {
            target.GetDamage(attackPoint);
        }
    }

    /* 원거리 범위 즉시 공격
     * 타겟 주변 반경 collider.Radius 안의 모든 타겟에 적용.
     * */
    public void LongDistanceSplashImmediatelyAttack(float attackPoint)
    {
        for(int i = 0; i < splashZone.targetsObjectInfo.Count; i++)
        {
            splashZone.targetsObjectInfo[splashZone.targetsIndex[i]].GetDamage(attackPoint);
        }
    }

    /* 오브젝트 비활성화시 실행되는 딕셔너리를 비우는 메소드.
     * */
    public void ClearTargets()
    {
        targets.Clear();
        targetsIndex.Clear();
    }

    /* 현재 타겟으로 들어와있는 대상이 올바른지 확인.
     * */
    public void TargetCheck()
    {
        for(int i = 0; i < targetsIndex.Count; i++)
        {
            if(!targetsObjectInfo[targetsIndex[i]].gameObject.activeInHierarchy)
            {
                targets.Remove(targetsIndex[i]);
                targetsObjectInfo.Remove(targetsIndex[i]);
                targetsIndex.RemoveAt(i);
            }
        }
    }
}
