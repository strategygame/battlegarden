﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class PlayerInfo : Singleton<PlayerInfo> {
    private string acountID;
    private InventoryScript inventory;
    private int amountOfSlots;
    private int winCount, defeatCount;
    private int pvPScore;
    private PvPGrade pvPGrade;

    private JSonManager jSonManager;
    private PlayerInfo selfPlayerInfo;
    

	// Use this for initialization
	void Start ()
    {
        inventory = Singleton<InventoryScript>.Instance;
        jSonManager = Singleton<JSonManager>.Instance;
        jSonManager.GetPlayerInfo();
        inventory.InitIneventory(amountOfSlots);
	}

    public void GetItem(int itemNo, int itemLevel)
    {
        inventory.AddItem(itemNo, itemLevel, 0, 0);
        jSonManager.UpdatePlayerInfo();
    }
}
