﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 타워의 정보를 관리하는 클래스.
 * 지금은 초기화값을 직접 입력해두지만 JSON 파일에서 받아오는 것으로 수정할 예정
 * */
public class TowerInfo
{
    public float healthPoint { get; set; }
    public bool canAttack { get; private set; }
    public float attackPoint { get; set; }
    public float attackRange { get; private set; }
    public float defensePoint { get; private set; }
    public float attackSpeed { get; set; }
    public AttackType attackType { get; private set; }
    public int towerBuildCost { get; private set; }
    public int towerMaintenanceCost { get; private set; }
    public int towerLevel { get; set; }
    public int towerLevelUpAddCost { get; private set; }
    public int towerLevelUpAddMaintenanceCost { get; private set; }
    public int spawnUnitAmount { get; set; }

    public TowerInfo(TowerType towerType)
    {
        if (towerType == TowerType.유닛생산) // 힐러 생산 건물
        {
            healthPoint = 100;
            canAttack = false;
            attackPoint = 0;
            attackRange = 0;
            attackSpeed = 0;
            towerBuildCost = 60;
            towerMaintenanceCost = 0;
            towerLevel = 1;
            towerLevelUpAddCost = 10;
            towerLevelUpAddMaintenanceCost = 0;
            spawnUnitAmount = 1;
        }
        else if (towerType == TowerType.방어타워) // 터렛
        {
            healthPoint = 300;
            canAttack = true;
            attackPoint = 10;
            attackRange = 6 * Mathf.Sqrt(5) / 3;
            defensePoint = 5;
            attackSpeed = 2;
            attackType = AttackType.발사체단일;
            towerBuildCost = 60;
            towerMaintenanceCost = 0;
            towerLevel = 1;
            towerLevelUpAddCost = 10;
            towerLevelUpAddMaintenanceCost = 0;
        }
        else if (towerType == TowerType.넥서스)
        {
            healthPoint = 1000;
            canAttack = false;
            attackPoint = 0;
            attackRange = 0;
            attackSpeed = 0;
            towerBuildCost = 50;
            towerMaintenanceCost = 0;
            towerLevel = 1;
            towerLevelUpAddCost = 20;
            towerLevelUpAddMaintenanceCost = 30;
        }
    }
}
