﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUImanager : Singleton<InventoryUImanager>
{
    private const int AtLeastAmountOfSlots = 18;
    private const int slotsPerRow = 6;
    private int amountOfSlots;
    private int slotIndexCount = 0;

    [SerializeField]
    GameObject inventoryContentView;
    [SerializeField]
    Button inventorySlotPrefab;

    List<Button> inventorySlots = new List<Button>();
    List<InventorySlotScript> inventorySlotScripts = new List<InventorySlotScript>();

    [SerializeField]
    Image IconView;
    [SerializeField]
    Text Description;

    public void AddNewSlot(ref List<InventorySlotScript> inventory, int amount)
    {
        
        Button newSlot = null;
        for (int i = 0; i < amount; i++)
        {
            newSlot = Instantiate(inventorySlotPrefab, inventoryContentView.transform, false);
            inventorySlots.Add(newSlot);
            inventorySlotScripts.Add(newSlot.GetComponent<InventorySlotScript>());
            inventory.Add(newSlot.GetComponent<InventorySlotScript>());
            newSlot.GetComponent<InventorySlotScript>().Init(slotIndexCount + i, slotIndexCount, i, slotsPerRow);
        }
        inventoryContentView.GetComponent<RectTransform>().sizeDelta = new Vector2(1180, (inventorySlots.Count / slotsPerRow + 1) * 200);
        inventoryContentView.GetComponent<RectTransform>().localPosition = new Vector3(0, 0, 0);
    }
}
