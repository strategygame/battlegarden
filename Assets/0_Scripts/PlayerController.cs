﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : Singleton<PlayerController>
{
    public Team team;
    private Vector2 mousePosition;
    int layerMask = 1 << 8;
    RaycastHit2D hit;
    TileScript currentSelectedTile;
    public bool canSelectTile;
    private int[] towerBuildCost = new int[6];
    private Touch tempTouchs;
    private Vector3 touchedPos;
    private bool touchOn;

    WalletScript myWallet;

    // 애셋번들로 바꿔줄 것.
    [SerializeField]
    GameObject[] towerPrefabs;

    // Use this for initialization
	void Start ()
    {
        Singleton<ObjectPoolScript>.Instance.ObjectPooling(towerPrefabs, team);
        SetTowerBuildCost(Singleton<ObjectPoolScript>.Instance.GetTowerBuildCost(team));
        canSelectTile = true;
        myWallet = GetComponent<WalletScript>();
        myWallet.Init(team);
        Singleton<CostManager>.Instance.Init(myWallet, team);
	}
	
	// Update is called once per frame
	void Update ()
    {
        touchOn = false;
        mousePosition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        // 타일 클릭 이벤트 발생시 패널 토글
        if(canSelectTile && Input.GetMouseButton(0))
        {
            hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(mousePosition), Vector2.zero, Mathf.Infinity, layerMask);
            if(hit.collider != null)
            {
                OnClickedTile();
            }
        }
        else if(canSelectTile && Input.touchCount > 0)
        {
            for(int i = 0; i < Input.touchCount; i++)
            {
                tempTouchs = Input.GetTouch(i);
                if(tempTouchs.phase == TouchPhase.Began)
                {
                    touchedPos = Camera.main.ScreenToWorldPoint(tempTouchs.position);
                    touchOn = true;

                    break;
                }
            }
        }
        if(touchOn)
        {
            hit = Physics2D.Raycast(touchedPos, Vector2.zero, Mathf.Infinity, layerMask);
            if(hit.collider != null)
            {
                OnClickedTile();
            }
        }
	}

    public void OnClickedTile()
    {
        TileScript tempHit = hit.collider.GetComponent<TileScript>();
        if (tempHit.team == team && tempHit.state == TileState.CanBuildTower && currentSelectedTile == null)
        {
            currentSelectedTile = tempHit;
            currentSelectedTile.ToggleTowerBuildCanvas();
            canSelectTile = false;
        }
    }

    public void BuildTower(int towerButtonIndex, Vector2 buildPosition)
    {
        if (myWallet.money >= towerBuildCost[towerButtonIndex])
        {
            Singleton<LevelManager>.Instance.BuildTower(towerButtonIndex, buildPosition, team);
            myWallet.AddMoney(-towerBuildCost[towerButtonIndex]);
            Singleton<CostManager>.Instance.UpdateUserMoneyText();
            currentSelectedTile = null;
            canSelectTile = true;
        }
    }

    public void CloseBuildTowerButton()
    {
        currentSelectedTile.ToggleTowerBuildCanvas();
        currentSelectedTile = null;
        canSelectTile = true;
    }

    public void SetTowerBuildCost(int[] towerBuildCost)
    {
        this.towerBuildCost = towerBuildCost;
    }
}
