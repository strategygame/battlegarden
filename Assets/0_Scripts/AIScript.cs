﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIScript : Singleton<AIScript>
{
    private Dictionary<Vector2, TowerScript> aITowers = new Dictionary<Vector2, TowerScript>();
    private List<Vector2> aITowersPosition = new List<Vector2>();
    [SerializeField]
    private GameObject[] towerPrefabs;
    private WalletScript myWallet;
    private Vector2 buildPosition;
    private Team team;
    private AIState state;
    private int needActionCount;
    private int nexusLevel;

    // Use this for initialization
    void Start()
    {
        team = Team.BTeam;
        Singleton<ObjectPoolScript>.Instance.ObjectPooling(towerPrefabs, team);
        state = AIState.UpgradeNexus;
        needActionCount = 1;
        myWallet = GetComponent<WalletScript>();
        myWallet.Init(team);
        Singleton<CostManager>.Instance.Init(myWallet, team);
    }

    // Update is called once per frame
    void Update()
    {
        AIAction();
    }

    private void AIAction()
    {
        if (state == AIState.UpgradeNexus)
        {
            if (UpgradeNexus())
            {
                needActionCount -= 1;
            }
            if (needActionCount <= 0)
            {
                if (aITowersPosition.Count <= 8)
                {
                    state = AIState.BuildTower;
                    if (nexusLevel % 2 == 0)
                    {
                        needActionCount = 1;
                    }
                    else
                    {
                        needActionCount = 2;
                    }
                }
                else
                {
                    state = AIState.UpgradeTower;
                    needActionCount = nexusLevel - 2;
                }
            }
        }
        else if (state == AIState.BuildTower)
        {
            if (BuildTower())
            {
                needActionCount -= 1;
            }
            if (needActionCount <= 0)
            {
                if (nexusLevel % 2 == 0 && nexusLevel < 5)
                {
                    state = AIState.UpgradeNexus;
                    needActionCount = 1;
                }
                else
                {
                    state = AIState.UpgradeTower;
                    needActionCount = nexusLevel / 2;
                }
            }
        }
        else if (state == AIState.UpgradeTower)
        {
            if (UpgradeTower())
            {
                needActionCount -= 1;
            }
            if (needActionCount <= 0 && nexusLevel < 5)
            {
                state = AIState.UpgradeNexus;
                needActionCount = 1;
            }
            else if (needActionCount <= 0)
            {
                needActionCount = 1;
            }
        }
    }

    private bool BuildTower()
    {
        int towerIndex = Random.Range(1, towerPrefabs.Length);
        buildPosition = new Vector2(Random.Range(11, 15), Random.Range(0, 4));
        if (Singleton<LevelManager>.Instance.tileDictionary[buildPosition].state == TileState.CanBuildTower && Singleton<LevelManager>.Instance.tileDictionary[buildPosition].team == team)
        {
            if (myWallet.money >= 60)
            {
                aITowers.Add(buildPosition, Singleton<LevelManager>.Instance.BuildTower(towerIndex, buildPosition, team));
                aITowersPosition.Add(buildPosition);
                return true;
            }
        }
        return false;
    }

    private bool UpgradeTower()
    {
        int tmpIndex = Random.Range(0, aITowersPosition.Count);
        TowerScript tower = aITowers[aITowersPosition[tmpIndex]];
        if (myWallet.money >= tower.GetUpgradeCost() && tower.GetTowerLevel() < 5)
        {
            myWallet.AddMoney(-tower.GetUpgradeCost());
            tower.UpgradeTower();
            return true;
        }
        return false;
    }

    private bool UpgradeNexus()
    {
        TowerScript nexus = Singleton<LevelManager>.Instance.towerDictionary[new Vector2(17, 1)];
        if (myWallet.money >= nexus.GetUpgradeCost() && nexus.GetTowerLevel() < 5)
        {
            myWallet.AddMoney(-nexus.GetUpgradeCost());
            nexus.UpgradeTower();
            nexusLevel = nexus.GetTowerLevel();
            return true;
        }
        return false;
    }

    public void DestroyedTower(ObjectInfo tower)
    {
        aITowersPosition.Remove(new Vector2(tower.gridPosition.x * 0.1f, tower.gridPosition.y * 0.1f));
        aITowers.Remove(new Vector2(tower.gridPosition.x * 0.1f, tower.gridPosition.y * 0.1f));

        if (state == AIState.BuildTower)
        {
            needActionCount++;
        }
        else if (state == AIState.UpgradeNexus || state == AIState.UpgradeTower)
        {
            state = AIState.BuildTower;
            needActionCount = 1;
        }
    }

    public void ReStartGame()
    {
        team = Team.BTeam;
        state = AIState.UpgradeNexus;
        needActionCount = 1;
        nexusLevel = 1;
    }
}
