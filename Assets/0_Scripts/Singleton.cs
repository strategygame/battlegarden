﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 싱글톤 구현을 위한 가상 클래스
 * */
public abstract class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    private static T instance;

    public static T Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<T>();
            }

            return instance;
        }
    }
}