﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{

    public Vector2 GridPosition { get; private set; }

    public Road roadRef { get; private set; }

    public Node Parent { get; private set; }

    public int G { get; set; }

    public int H { get; set; }

    public int F { get; set; }

    public Node(Road roadRef)
    {
        this.roadRef = roadRef;
        this.GridPosition = roadRef.gridPosition;
    }

    public void CalcValues(Node parent, Node goal, int gCost)
    {
        this.Parent = parent;
        this.G = parent.G + gCost;
        this.H = (int)((Mathf.Abs((GridPosition.x - goal.GridPosition.x)) + Mathf.Abs((GridPosition.y - goal.GridPosition.y))) * 10);
        this.F = G + H;
    }
}
