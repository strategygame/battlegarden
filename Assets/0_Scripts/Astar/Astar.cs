﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class Astar
{
    private static Dictionary<Vector2, Node> nodes;

    private static void CreateNodes()
    {
        nodes = new Dictionary<Vector2, Node>();

        foreach (Road road in LevelManager.Instance.roadDictionary.Values)
        {
            nodes.Add(road.gridPosition, new Node(road));
        }
    }

    public static Stack<Node> GetPath(Vector2 start, Vector2 goal)
    {
        if (nodes == null)
        {
            CreateNodes();
        }

        //Creates an open list to be used with the A* algorithm
        HashSet<Node> openList = new HashSet<Node>();

        //Creates an closed list to be used with the A* algorithm
        HashSet<Node> closedList = new HashSet<Node>();

        // 1, 2, 3

        Stack<Node> finalPath = new Stack<Node>();

        Node currentNode = nodes[start];
        //1. Adds the start node to the OpenList
        openList.Add(currentNode);

        while (openList.Count > 0)//Step 10
        {
            //2. Runs through all neighbors
            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    //Currnet node = x10,y10 = 9, 9
                    Vector2 neighbourPos = new Vector2(currentNode.GridPosition.x - x, currentNode.GridPosition.y - y);

                    if (LevelManager.Instance.InBounds(neighbourPos) && LevelManager.Instance.roadDictionary[neighbourPos].walkable && neighbourPos != currentNode.GridPosition)
                    {
                        int gCost = 0;

                        if (Mathf.Abs(x - y) == 1)
                        {
                            gCost = 10;
                        }
                        else
                        {
                            if (!ConnectedDiagonally(currentNode, nodes[neighbourPos]))
                            {
                                continue;
                            }
                            gCost = 14;
                        }

                        //3. Adds the neighbor to the open list
                        Node neighbour = nodes[neighbourPos];

                        if (openList.Contains(neighbour))
                        {
                            if (currentNode.G + gCost < neighbour.G)//9.4
                            {
                                neighbour.CalcValues(currentNode, nodes[goal], gCost);
                            }
                        }
                        else if (!closedList.Contains(neighbour))//9.1
                        {
                            openList.Add(neighbour); //9.2
                            neighbour.CalcValues(currentNode, nodes[goal], gCost);//9.3
                        }
                    }
                }
            }

            //5. & 8. Moves the current node from the open list to the closed list
            openList.Remove(currentNode);
            closedList.Add(currentNode);

            if (openList.Count > 0)//STEP. 7
            {
                //Sorts the list by F value, and selects the first on the list
                currentNode = openList.OrderBy(n => n.F).First();
            }

            if (currentNode == nodes[goal])
            {
                while (currentNode.GridPosition != start)
                {
                    finalPath.Push(currentNode);
                    currentNode = currentNode.Parent;
                }
                break;
            }
        }

        return finalPath;

        //****THIS IS ONLY FOR DEBUGGING NEEDS TO BE REMOVED LATER!*****
        // GameObject.Find("AStarDebugger").GetComponent<AStarDebugger>().DebugPath(openList, closedList, finalPath);
    }

    private static bool ConnectedDiagonally(Node currentNode, Node neighbor)
    {
        Vector2 direction = neighbor.GridPosition - currentNode.GridPosition;

        Vector2 first = new Vector2(currentNode.GridPosition.x + direction.x, currentNode.GridPosition.y);

        Vector2 second = new Vector2(currentNode.GridPosition.x, currentNode.GridPosition.y + direction.y);

        if (LevelManager.Instance.InBounds(first) && !LevelManager.Instance.roadDictionary[first].walkable)
        {
            return false;
        }
        if (LevelManager.Instance.InBounds(second) && !LevelManager.Instance.roadDictionary[second].walkable)
        {
            return false;
        }

        return true;
    }
}
