﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 오브젝트 풀 관리용 클래스.
 * 가비지콜렉터로부터 해방 받기 위하여 필요하다.
 * */
public class ObjectPoolScript : Singleton<ObjectPoolScript> {
    private const int towerPoolingCount = 12;
    private const int unitPoolingCount = 50;
    private const int projectilePoolingCount = 100;

    public static List<List<GameObject>> ATeamTowerObjects = new List<List<GameObject>>();
    public static List<List<GameObject>> BTeamTowerObjects = new List<List<GameObject>>();
    public static List<List<GameObject>> ATeamUnitObjects = new List<List<GameObject>>();
    public static List<List<GameObject>> BTeamUnitObjects = new List<List<GameObject>>();
    public static List<List<GameObject>> ATeamProjectileObjects = new List<List<GameObject>>();
    public static List<List<GameObject>> BTeamProjectileObjects = new List<List<GameObject>>();

    public static List<Stack<int>> SpawnableATeamTowerObjectsIndex = new List<Stack<int>>();
    public static List<Stack<int>> SpawnableBTeamTowerObjectsIndex = new List<Stack<int>>();
    public static List<Stack<int>> SpawnableATeamUnitObjectsIndex = new List<Stack<int>>();
    public static List<Stack<int>> SpawnableBTeamUnitObjectsIndex = new List<Stack<int>>();
    public static List<Stack<int>> SpawnableATeamProjectileObjectsIndex = new List<Stack<int>>();
    public static List<Stack<int>> SpawnableBTeamProjectileObjectsIndex = new List<Stack<int>>();

    public static GameObject ATeamNexus, BTeamNexus;

    /* 인게임 시작할 때 오브젝트 풀링 메소드.
     * 인게임이 시작할 때 실행 함.
     * 적당히 부족하지 않을 정도의 수를 풀링하도록 한다.
     * 부족할 경우 풀을 추가하는 메소드를 실행하여 그 때 그 때 충당하도록 하며,
     * 너무 자주 추가하는 일이 발생하여 CPU에 부하가 생기지 않도록 충분한 수를 초기에 풀링하도록 한다.
     * */
    public void ObjectPooling(GameObject[] towerObjects, Team team)
    {
        /* ATeam의 Object들을 Pooling
         * */
        if(team == Team.ATeam)
        {
            for(int i = 0; i < towerObjects.Length; i++)
            {
                ATeamTowerObjects.Add(new List<GameObject>());
                SpawnableATeamTowerObjectsIndex.Add(new Stack<int>());
                if (towerObjects[i] != null)
                {
                    for (int j = 0; j < towerPoolingCount; j++)
                    {
                        ATeamTowerObjects[i].Add(Instantiate(towerObjects[i]));
                        ATeamTowerObjects[i][j].GetComponent<TowerScript>().Init(team, new int[2] { i, j });
                        Singleton<LevelManager>.Instance.AddObjectInfoDic(ATeamTowerObjects[i][j].GetComponent<ObjectInfo>());
                        ATeamTowerObjects[i][j].SetActive(false);
                        SpawnableATeamTowerObjectsIndex[i].Push(j);
                    }
                }
            }

            for(int i = 0; i < towerObjects.Length; i++)
            {
                ATeamUnitObjects.Add(new List<GameObject>());
                SpawnableATeamUnitObjectsIndex.Add(new Stack<int>());
                if(ATeamTowerObjects[i].Count != 0)
                {
                    GameObject tempUnit = ATeamTowerObjects[i][0].GetComponent<TowerScript>().GetUnitPrefab();
                    if (tempUnit != null && ATeamTowerObjects[i][0].GetComponent<TowerScript>().GetTowerType() != TowerType.방어타워)
                    {
                        for (int j = 0; j < unitPoolingCount; j++)
                        {
                            ATeamUnitObjects[i].Add(Instantiate(tempUnit));
                            ATeamUnitObjects[i][j].GetComponent<UnitScript>().Init(team, new int[2] { i, j });
                            Singleton<LevelManager>.Instance.AddObjectInfoDic(ATeamUnitObjects[i][j].GetComponent<ObjectInfo>());
                            ATeamUnitObjects[i][j].SetActive(false);
                            SpawnableATeamUnitObjectsIndex[i].Push(j);
                        }
                    }
                }
            }

            for(int i = 0; i < towerObjects.Length; i++)
            {
                ATeamProjectileObjects.Add(new List<GameObject>());
                SpawnableATeamProjectileObjectsIndex.Add(new Stack<int>());
                if(ATeamUnitObjects[i].Count != 0)
                {
                    GameObject tempProjectile = ATeamUnitObjects[i][0].GetComponent<UnitScript>().GetProjectileObject();
                    if(tempProjectile != null && ATeamUnitObjects[i][0].GetComponent<UnitScript>().GetHasProjectile())
                    {
                        bool isHealer = ATeamUnitObjects[i][0].GetComponent<UnitScript>().GetIsHealer();
                        for(int j = 0; j < projectilePoolingCount; j++)
                        {
                            ATeamProjectileObjects[i].Add(Instantiate(tempProjectile));
                            if (isHealer)
                            {
                                ATeamProjectileObjects[i][j].GetComponent<ProjectileScript>().Init(Team.ATeam, Team.ATeam, new int[] { i, j });
                            }
                            else
                            {
                                ATeamProjectileObjects[i][j].GetComponent<ProjectileScript>().Init(Team.BTeam, Team.ATeam, new int[] { i, j });
                            }
                            ATeamProjectileObjects[i][j].SetActive(false);
                            SpawnableATeamProjectileObjectsIndex[i].Push(j);
                        }
                    }
                }
            }
        }

        /* BTeam의 Object들을 Pooling
         * */
        else if(team == Team.BTeam)
        {
            for (int i = 0; i < towerObjects.Length; i++)
            {
                BTeamTowerObjects.Add(new List<GameObject>());
                SpawnableBTeamTowerObjectsIndex.Add(new Stack<int>());
                if (towerObjects[i] != null)
                {
                    for (int j = 0; j < towerPoolingCount; j++)
                    {
                        BTeamTowerObjects[i].Add(Instantiate(towerObjects[i]));
                        BTeamTowerObjects[i][j].GetComponent<TowerScript>().Init(team, new int[2] { i, j });
                        Singleton<LevelManager>.Instance.AddObjectInfoDic(BTeamTowerObjects[i][j].GetComponent<ObjectInfo>());
                        BTeamTowerObjects[i][j].SetActive(false);
                        SpawnableBTeamTowerObjectsIndex[i].Push(j);
                    }
                }
            }

            for (int i = 0; i < towerObjects.Length; i++)
            {
                BTeamUnitObjects.Add(new List<GameObject>());
                SpawnableBTeamUnitObjectsIndex.Add(new Stack<int>());
                if (BTeamTowerObjects[i].Count != 0)
                {
                    GameObject tempUnit = BTeamTowerObjects[i][0].GetComponent<TowerScript>().GetUnitPrefab();
                    if (tempUnit != null && BTeamTowerObjects[i][0].GetComponent<TowerScript>().GetTowerType() != TowerType.방어타워)
                    {
                        for (int j = 0; j < unitPoolingCount; j++)
                        {
                            BTeamUnitObjects[i].Add(Instantiate(tempUnit));
                            BTeamUnitObjects[i][j].GetComponent<UnitScript>().Init(team, new int[2] { i, j });
                            Singleton<LevelManager>.Instance.AddObjectInfoDic(BTeamUnitObjects[i][j].GetComponent<ObjectInfo>());
                            BTeamUnitObjects[i][j].SetActive(false);
                            SpawnableBTeamUnitObjectsIndex[i].Push(j);
                        }
                    }
                }
            }

            for (int i = 0; i < towerObjects.Length; i++)
            {
                BTeamProjectileObjects.Add(new List<GameObject>());
                SpawnableBTeamProjectileObjectsIndex.Add(new Stack<int>());
                if (BTeamUnitObjects[i].Count != 0)
                {
                    GameObject tempProjectile = BTeamUnitObjects[i][0].GetComponent<UnitScript>().GetProjectileObject();
                    if (tempProjectile != null && BTeamUnitObjects[i][0].GetComponent<UnitScript>().GetHasProjectile())
                    {
                        bool isHealer = BTeamUnitObjects[i][0].GetComponent<UnitScript>().GetIsHealer();
                        for (int j = 0; j < projectilePoolingCount; j++)
                        {
                            BTeamProjectileObjects[i].Add(Instantiate(tempProjectile));
                            if (isHealer)
                            {
                                BTeamProjectileObjects[i][j].GetComponent<ProjectileScript>().Init(Team.BTeam, Team.BTeam, new int[] { i, j });
                            }
                            else
                            {
                                BTeamProjectileObjects[i][j].GetComponent<ProjectileScript>().Init(Team.ATeam, Team.BTeam, new int[] { i, j });
                            }
                            BTeamProjectileObjects[i][j].SetActive(false);
                            SpawnableBTeamProjectileObjectsIndex[i].Push(j);
                        }
                    }
                }
            }
        }
        
    }

    public void NexusPooling(GameObject nexus)
    {
        ATeamNexus = Instantiate(nexus);
        ATeamNexus.GetComponent<TowerScript>().Init(Team.ATeam, new int[2] { 0, 0 });
        Singleton<LevelManager>.Instance.AddObjectInfoDic(ATeamNexus.GetComponent<ObjectInfo>());
        BTeamNexus = Instantiate(nexus);
        BTeamNexus.GetComponent<TowerScript>().Init(Team.BTeam, new int[2] { 0, 0 });
        Singleton<LevelManager>.Instance.AddObjectInfoDic(BTeamNexus.GetComponent<ObjectInfo>());
    }

    public void NexusSetPosition(Vector2 position, Team team)
    {
        if(team == Team.ATeam)
        {
            TowerScript temp = ATeamNexus.GetComponent<TowerScript>();
            temp.SetPosition(position);
            Singleton<LevelManager>.Instance.towerDictionary.Add(position, temp);
        }
        else if(team == Team.BTeam)
        {
            TowerScript temp = BTeamNexus.GetComponent<TowerScript>();
            temp.SetPosition(position);
            Singleton<LevelManager>.Instance.towerDictionary.Add(position, temp);
        }
    }

    /* 풀에 있는 건물을 활성화 시켜주는 메소드.
     * 풀에 남아있는 오브젝트가 없을 경우 일정 개수를 추가하여 활용하도록 한다.
     * */
    public GameObject SpawnTowerObject(int towerIndex, Team team)
    {
        GameObject tempTower = null;
        if(team == Team.ATeam)
        {
            if(SpawnableATeamTowerObjectsIndex[towerIndex].Count > 0)
            {
                tempTower = ATeamTowerObjects[towerIndex][SpawnableATeamTowerObjectsIndex[towerIndex].Peek()];
                tempTower.transform.position = new Vector2(0, 20);
                ATeamTowerObjects[towerIndex][SpawnableATeamTowerObjectsIndex[towerIndex].Pop()].SetActive(true);
            }
            else
            {
                for(int i = 0; i < 3; i++)
                {
                    ATeamTowerObjects[towerIndex].Add(Instantiate(ATeamTowerObjects[towerIndex][0]));
                    ATeamTowerObjects[towerIndex][ATeamTowerObjects[towerIndex].Count - 1].SetActive(false);
                    ATeamTowerObjects[towerIndex][ATeamTowerObjects[towerIndex].Count - 1].GetComponent<TowerScript>().Init
                        (team, new int[] { towerIndex, ATeamTowerObjects[towerIndex].Count - 1});
                    Singleton<LevelManager>.Instance.AddObjectInfoDic(ATeamTowerObjects[towerIndex][ATeamTowerObjects[towerIndex].Count - 1].GetComponent<ObjectInfo>());
                    SpawnableATeamTowerObjectsIndex[towerIndex].Push(ATeamTowerObjects[towerIndex].Count - 1);
                }
                tempTower = ATeamTowerObjects[towerIndex][SpawnableATeamTowerObjectsIndex[towerIndex].Peek()];
                tempTower.transform.position = new Vector2(0, 20);
                ATeamTowerObjects[towerIndex][SpawnableATeamTowerObjectsIndex[towerIndex].Pop()].SetActive(true);
            }
        }

        else if(team == Team.BTeam)
        {
            if(SpawnableBTeamTowerObjectsIndex[towerIndex].Count > 0)
            {
                tempTower = BTeamTowerObjects[towerIndex][SpawnableBTeamTowerObjectsIndex[towerIndex].Peek()];
                tempTower.transform.position = new Vector2(20, 20);
                BTeamTowerObjects[towerIndex][SpawnableBTeamTowerObjectsIndex[towerIndex].Pop()].SetActive(true);
            }
            else
            {
                for (int i = 0; i < 3; i++)
                {
                    BTeamTowerObjects[towerIndex].Add(Instantiate(BTeamTowerObjects[towerIndex][0]));
                    BTeamTowerObjects[towerIndex][BTeamTowerObjects[towerIndex].Count - 1].SetActive(false);
                    BTeamTowerObjects[towerIndex][BTeamTowerObjects[towerIndex].Count - 1].GetComponent<TowerScript>().Init
                        (team, new int[] { towerIndex, BTeamTowerObjects[towerIndex].Count - 1 });
                    Singleton<LevelManager>.Instance.AddObjectInfoDic(ATeamTowerObjects[towerIndex][BTeamTowerObjects[towerIndex].Count - 1].GetComponent<ObjectInfo>());
                    SpawnableBTeamTowerObjectsIndex[towerIndex].Push(BTeamTowerObjects[towerIndex].Count - 1);
                }
                tempTower = BTeamTowerObjects[towerIndex][SpawnableBTeamTowerObjectsIndex[towerIndex].Peek()];
                tempTower.transform.position = new Vector2(20, 20);
                BTeamTowerObjects[towerIndex][SpawnableBTeamTowerObjectsIndex[towerIndex].Pop()].SetActive(true);
            }
        }
        
        return tempTower;
    }

    public int[] GetTowerBuildCost(Team team)
    {
        int[] buildcost = new int[6];

        if (team == Team.ATeam)
        {
            for (int i = 0; i < ATeamTowerObjects.Count; i++)
            {
                if(ATeamTowerObjects[i].Count > 0)
                {
                    buildcost[i] = ATeamTowerObjects[i][0].GetComponent<TowerScript>().GetBuildCost();
                }
            }
        }
        else if (team == Team.BTeam)
        {
            for (int i = 0; i < BTeamTowerObjects.Count; i++)
            {
                if (BTeamTowerObjects[i].Count > 0)
                {
                    buildcost[i] = BTeamTowerObjects[i][0].GetComponent<TowerScript>().GetBuildCost();
                }
            }
        }
        Debug.Log(buildcost);
        return buildcost;
    }

    public GameObject SpawnUnitObject(int unitIndex, Team team)
    {
        GameObject tempUnitObject = null;
        if(team == Team.ATeam)
        {
            if(SpawnableATeamUnitObjectsIndex[unitIndex].Count > 0)
            {
                tempUnitObject = ATeamUnitObjects[unitIndex][SpawnableATeamUnitObjectsIndex[unitIndex].Pop()];
                tempUnitObject.transform.position = new Vector2(0, 20);
                tempUnitObject.SetActive(true);
            }
            else
            {
                for(int i = 0; i < 3; i++)
                {
                    ATeamUnitObjects[unitIndex].Add(Instantiate(ATeamUnitObjects[unitIndex][0]));
                    ATeamUnitObjects[unitIndex][ATeamUnitObjects[unitIndex].Count - 1].GetComponent<UnitScript>().Init(Team.ATeam, new int[] { unitIndex, ATeamUnitObjects[unitIndex].Count - 1 });
                    Singleton<LevelManager>.Instance.AddObjectInfoDic(ATeamUnitObjects[unitIndex][ATeamUnitObjects[unitIndex].Count - 1].GetComponent<ObjectInfo>());
                    ATeamUnitObjects[unitIndex][ATeamUnitObjects[unitIndex].Count - 1].SetActive(false);
                    SpawnableATeamUnitObjectsIndex[unitIndex].Push(ATeamUnitObjects[unitIndex].Count - 1);
                }
                tempUnitObject = ATeamUnitObjects[unitIndex][SpawnableATeamUnitObjectsIndex[unitIndex].Pop()];
                tempUnitObject.transform.position = new Vector2(0, 20);
                tempUnitObject.SetActive(true);
            }
        }
        else if(team == Team.BTeam)
        {
            if(SpawnableBTeamUnitObjectsIndex[unitIndex].Count > 0)
            {
                tempUnitObject = BTeamUnitObjects[unitIndex][SpawnableBTeamUnitObjectsIndex[unitIndex].Pop()];
                tempUnitObject.transform.position = new Vector2(20, 20);
                tempUnitObject.SetActive(true);
            }
            else
            {
                for(int i = 0; i < 3; i++)
                {
                    BTeamUnitObjects[unitIndex].Add(Instantiate(BTeamUnitObjects[unitIndex][0]));
                    BTeamUnitObjects[unitIndex][BTeamUnitObjects[unitIndex].Count - 1].GetComponent<UnitScript>().Init(Team.BTeam, new int[] { unitIndex, ATeamUnitObjects[unitIndex].Count - 1 });
                    Singleton<LevelManager>.Instance.AddObjectInfoDic(BTeamUnitObjects[unitIndex][BTeamUnitObjects[unitIndex].Count - 1].GetComponent<ObjectInfo>());
                    BTeamUnitObjects[unitIndex][BTeamUnitObjects[unitIndex].Count - 1].SetActive(false);
                    SpawnableBTeamUnitObjectsIndex[unitIndex].Push(BTeamUnitObjects[unitIndex].Count - 1);
                }
                tempUnitObject = BTeamUnitObjects[unitIndex][SpawnableBTeamUnitObjectsIndex[unitIndex].Pop()];
                tempUnitObject.transform.position = new Vector2(20, 20); 
                tempUnitObject.SetActive(true);
            }
        }
        return tempUnitObject;
    }

    public GameObject SpawnProjectileObject(int objectIndex, Team team)
    {
        GameObject tempProjectileObject = null;

        if(team == Team.ATeam)
        {
            if(SpawnableATeamProjectileObjectsIndex[objectIndex].Count > 0)
            {
                tempProjectileObject = ATeamProjectileObjects[objectIndex][SpawnableATeamProjectileObjectsIndex[objectIndex].Pop()];
                tempProjectileObject.transform.position = new Vector2(0, 20);
                tempProjectileObject.SetActive(true);
            }

            else
            {
                for(int i = 0; i < 5; i++)
                {
                    ATeamProjectileObjects[objectIndex].Add(Instantiate(ATeamProjectileObjects[objectIndex][0]));
                    ATeamProjectileObjects[objectIndex][ATeamProjectileObjects[objectIndex].Count - 1].GetComponent<ProjectileScript>().Init
                        (ATeamProjectileObjects[objectIndex][0].GetComponent<ProjectileScript>().GetTargetTeam(), Team.ATeam,
                        new int[] { objectIndex, ATeamProjectileObjects[objectIndex].Count - 1 });
                    ATeamProjectileObjects[objectIndex][ATeamProjectileObjects[objectIndex].Count - 1].SetActive(false);
                    SpawnableATeamProjectileObjectsIndex[objectIndex].Push(ATeamProjectileObjects[objectIndex].Count - 1);
                }

                tempProjectileObject = ATeamProjectileObjects[objectIndex][SpawnableATeamProjectileObjectsIndex[objectIndex].Pop()];
                tempProjectileObject.transform.position = new Vector2(0, 20);
                tempProjectileObject.SetActive(true);
            }
        }

        else if(team == Team.BTeam)
        {
            if(SpawnableBTeamProjectileObjectsIndex[objectIndex].Count > 0)
            {
                tempProjectileObject = BTeamProjectileObjects[objectIndex][SpawnableBTeamProjectileObjectsIndex[objectIndex].Pop()];
                tempProjectileObject.transform.position = new Vector2(0, 20);
                tempProjectileObject.SetActive(true);
            }

            else
            {
                for (int i = 0; i < 5; i++)
                {
                    BTeamProjectileObjects[objectIndex].Add(Instantiate(BTeamProjectileObjects[objectIndex][0]));
                    BTeamProjectileObjects[objectIndex][BTeamProjectileObjects[objectIndex].Count - 1].GetComponent<ProjectileScript>().Init
                        (BTeamProjectileObjects[objectIndex][0].GetComponent<ProjectileScript>().GetTargetTeam(), Team.BTeam,
                        new int[] { objectIndex, BTeamProjectileObjects[objectIndex].Count - 1 });
                    BTeamProjectileObjects[objectIndex][BTeamProjectileObjects[objectIndex].Count - 1].SetActive(false);
                    SpawnableBTeamProjectileObjectsIndex[objectIndex].Push(BTeamProjectileObjects[objectIndex].Count - 1);
                }

                tempProjectileObject = BTeamProjectileObjects[objectIndex][SpawnableBTeamProjectileObjectsIndex[objectIndex].Pop()];
                tempProjectileObject.transform.position = new Vector2(0, 20);
                tempProjectileObject.SetActive(true);
            }
        }

        return tempProjectileObject;
    }

    public void DeSpawnUnitObject(int[] unitIndex, Team team)
    {
        if(team == Team.ATeam)
        {
            SpawnableATeamUnitObjectsIndex[unitIndex[0]].Push(unitIndex[1]);
        }
        else if(team == Team.BTeam)
        {
            SpawnableBTeamUnitObjectsIndex[unitIndex[0]].Push(unitIndex[1]);
        }
    }

    public void DeSpawnTowerObject(int[] towerIndex, Team team)
    {
        if(team == Team.ATeam)
        {
            SpawnableATeamTowerObjectsIndex[towerIndex[0]].Push(towerIndex[1]);
        }
        else if(team == Team.BTeam)
        {
            SpawnableBTeamTowerObjectsIndex[towerIndex[0]].Push(towerIndex[1]);
        }
    }

    public void DeSpawnProjectileObject(int[] objectIndex, Team team)
    {
        if(team == Team.ATeam)
        {
            SpawnableATeamProjectileObjectsIndex[objectIndex[0]].Push(objectIndex[1]);
        }
        else if(team == Team.BTeam)
        {
            SpawnableBTeamProjectileObjectsIndex[objectIndex[0]].Push(objectIndex[1]);
        }
    }
}
