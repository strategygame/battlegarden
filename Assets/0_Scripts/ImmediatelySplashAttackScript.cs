﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmediatelySplashAttackScript : MonoBehaviour
{
    private Team targetTeam;

    public GameObject mainTarget;

    public Dictionary<int, GameObject> targets = new Dictionary<int, GameObject>();
    public List<int> targetsIndex = new List<int>();
	
	
	void Update ()
    {
		if(mainTarget != null)
        {
            transform.position = mainTarget.transform.position;
        }
	}

    public void SplashAttack(float attackPoint)
    {
        for(int i = 0; i < targetsIndex.Count; i++)
        {
            if(targets[targetsIndex[i]].CompareTag("Bunilding"))
            {
                targets[targetsIndex[i]].GetComponent<TowerScript>().GetDamage(attackPoint);
            }

            else if(targets[targetsIndex[i]].CompareTag("Unit"))
            {
                targets[targetsIndex[i]].GetComponent<UnitScript>().GetDamage(attackPoint);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Building") || other.CompareTag("Unit"))
        {
            targets.Add(other.GetComponent<ObjectInfo>().objectCount, other.gameObject);
            targetsIndex.Add(other.GetComponent<ObjectInfo>().objectCount);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.CompareTag("Building") || other.CompareTag("Unit"))
        {
            targets.Remove(other.GetComponent<ObjectInfo>().objectCount);
            targetsIndex.Remove(other.GetComponent<ObjectInfo>().objectCount);
        }
    }
}
