﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Road
{
    public Vector2 gridPosition { get; private set; }
    public Vector2 worldPosition { get; private set; }
    public TileState state { get; private set; }
    public bool walkable { get; private set; }

    public Road(Vector2 gridPosition, Vector2 worldPosition, TileState newState)
    {
        this.gridPosition = gridPosition;
        this.worldPosition = worldPosition;
        this.state = newState;
        if (newState == TileState.Default || newState == TileState.Road || newState == TileState.CanBuildTower)
        {
            walkable = true;
        }
        else
        {
            walkable = false;
        }
    }

    public void SetState(TileState state)
    {
        this.state = state;
        if (state == TileState.Default || state == TileState.Road || state == TileState.CanBuildTower)
        {
            walkable = true;
        }
        else
        {
            walkable = false;
        }
    }
}
