﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* 타일을 제외한 모든 오브젝트에 들어가는 구조체 클래스.
 * ㄴ다른 오브젝트를 판별할 때 대상 오브젝트의 클래스를 구분하기 위해 사용.
 * */
public class ObjectInfo : MonoBehaviour
{
    public int objectCount;
    public bool isUnit;
    public Vector2 gridPosition = new Vector2();
    public Team team;
    private TowerScript myTowerScript;
    private UnitScript myUnitScript;

    void Awake()
    {
        if(isUnit)
        {
            myUnitScript = gameObject.GetComponent<UnitScript>();
        }
        else
        {
            myTowerScript = gameObject.GetComponent<TowerScript>();
        }
    }

    public void GetDamage(float damagePoint)
    {
        if(isUnit)
        {
            myUnitScript.GetDamage(damagePoint);
        }
        else
        {
            myTowerScript.GetDamage(damagePoint);
        }
    }

    public float GetDamagedPoint()
    {
        if(isUnit)
        {
            return myUnitScript.GetDamagedPoint();
        }
        else
        {
            return 0;
        }
    }
}
