﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CostManager : Singleton<CostManager>
{
    [SerializeField]
    private Text userMoneyText;
    private WalletScript aTeamWallet, bTeamWallet;

    public void Init(WalletScript wallet, Team team)
    {
        if(team == Team.ATeam)
        {
            this.aTeamWallet = wallet;
            UpdateUserMoneyText();
        }
        else if(team == Team.BTeam)
        {
            this.bTeamWallet = wallet;
        }
    }

    public void AddMoney(int add, Team team)
    {
        if (team == Team.ATeam)
        {
            aTeamWallet.AddMoney(add);
            UpdateUserMoneyText();
        }
        else if (team == Team.BTeam)
        {
            bTeamWallet.AddMoney(add);
        }
    }

    /* 유저가 ATeam이 아닐 수 있는 경우가 나오면 수정 해줄 것.
     * */
    public void UpdateUserMoneyText()
    {
        userMoneyText.text = "Gold : " + aTeamWallet.money;
    }

    public void AddMoneyIncome(int add, Team team)
    {
        if (team == Team.ATeam)
        {
            aTeamWallet.AddIncome(add);
        }
        else if (team == Team.BTeam)
        {
            bTeamWallet.AddIncome(add);
        }
    }

    public void GetIncome(Team team)
    {
        if(team == Team.ATeam)
        {
            aTeamWallet.AddMoney(aTeamWallet.income);
            UpdateUserMoneyText();
        }
        else if(team == Team.BTeam)
        {
            bTeamWallet.AddMoney(bTeamWallet.income);
        }
    }

    public int GetMoney(Team team)
    {
        if(team == Team.ATeam)
        {
            return aTeamWallet.money;
        }
        else if(team == Team.BTeam)
        {
            return bTeamWallet.money;
        }

        return -1;
    }

    //public void ReStartGame()
    //{
    //    UpdateUserMoneyText();
    //}
}
