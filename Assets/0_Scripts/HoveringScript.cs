﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoveringScript : MonoBehaviour
{
    private SpriteRenderer theSpriteRenderer;
    [SerializeField]
    private Sprite[] towerIcons;
    private TowerType iconType;

    // Use this for initialization
    void Start()
    {
        theSpriteRenderer = GetComponent<SpriteRenderer>();
        towerIcons = Resources.LoadAll<Sprite>("1_Texture/testTowersAndUnitsIcon");
    }

    // Update is called once per frame
    void Update()
    {
        HoveringMouse();
        UpdateIcon();
    }

    void HoveringMouse()
    {
        this.transform.position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        this.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }

    public void SetIconType(TowerType newIcon)
    {
        iconType = newIcon;
    }

    private void UpdateIcon()
    {
        if ((int)iconType > 0)
        {
            theSpriteRenderer.sprite = towerIcons[((int)iconType - 1) * 2];
        }
        else
        {
            theSpriteRenderer.sprite = null;
        }
    }
}
