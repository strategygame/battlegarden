﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlotScript : MonoBehaviour
{
    private int slotIndex;
    private bool hasItem;
    private bool isEquipped;
    private ItemInfoStruct itemInfo;

    [SerializeField]
    private Text itemName;

    public void Init(int slotIndex, int slotIndexCount, int i, int slotsPerRow)
    {
        this.slotIndex = slotIndex;
        transform.localPosition = new Vector3((((slotIndexCount + i) % slotsPerRow) * 2 + 1) * 100, -(((slotIndexCount + i) / slotsPerRow) * 2 + 1) * 100, 0);
    }

    public int GetSlotIndex()
    {
        return slotIndex;
    }

    public int GetItemNo()
    {
        return itemInfo.itemNo;
    }

    public bool GetHasItem()
    {
        return this.hasItem;
    }

    public bool GetIsEquipped()
    {
        return this.isEquipped;
    }

    public void SetIsEquipped(bool isEquipped)
    {
        this.isEquipped = isEquipped;
    }

    /* 아이템 획득
     * */
    public void AddItem(ItemInfoStruct itemInfo, int currentLevelExp, int currentGradeExp)
    {
        this.itemInfo = itemInfo;
        this.hasItem = true;
        itemName.text = itemInfo.name;
    }

    /* 아이템 사용.
     * */
    public void UseItem()
    {
        this.hasItem = false;
    }

    /* 아이템 업그레이드
     * */
    public void UpgradeItem(int addExp)
    {
        
    }

    /* 아이템 레벨업
     * */
    public void LevelUpItem(int addExp)
    {

    }
}
