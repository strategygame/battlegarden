﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashAttackScript : MonoBehaviour
{
    private Team targetTeam;

    public Dictionary<int, GameObject> targets = new Dictionary<int, GameObject>();
    public Dictionary<int, ObjectInfo> targetsObjectInfo = new Dictionary<int, ObjectInfo>();
    public List<int> targetsIndex = new List<int>();


    private void InitSpawned(Team targetTeam)
    {
        this.targetTeam = targetTeam;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        ObjectInfo newTarget = other.transform.parent.GetComponent<ObjectInfo>();
        if(newTarget.team == targetTeam)
        {
            targets.Add(newTarget.objectCount, newTarget.gameObject);
            targetsObjectInfo.Add(newTarget.objectCount, newTarget);
            targetsIndex.Add(newTarget.objectCount);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        ObjectInfo outTarget = other.transform.parent.GetComponent<ObjectInfo>();
        if (outTarget.team == targetTeam)
        {
            targets.Remove(outTarget.objectCount);
            targetsObjectInfo.Remove(outTarget.objectCount);
            targetsIndex.Remove(outTarget.objectCount);
        }
    }

    public void SetTargetTeam(Team targetTeam)
    {
        this.targetTeam = targetTeam;
    }

    public void DictionaryClear()
    {
        targets.Clear();
        targetsIndex.Clear();
        targetsObjectInfo.Clear();
    }
}
