﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/* 씬에 하나만 존재해야 하므로 싱글톤 클래스를 상속받도록 한다.
 * */
public class GameManager : Singleton<GameManager>
{
    private float ATeamTurnTime, BTeamTurnTime;
    private float timeCount;
    private Turn turn;
    private List<EventTrigger> triggers = new List<EventTrigger>();
    private List<EventTrigger.Entry> entrys = new List<EventTrigger.Entry>();

    [SerializeField]
    private GameObject nexusPrefab;

    /* 변수 초기화, 이벤트 트리거 리스트에 각 버튼의 이벤트트리거 등록.
     * */
    void Awake ()
    {
        ATeamTurnTime = 7.5f;
        BTeamTurnTime = 0.0f;
        timeCount = 0;
        turn = Turn.ATeam;
        

    }

    void Start()
    {
        Singleton<ObjectPoolScript>.Instance.NexusPooling(nexusPrefab);
        SetNexusPosition();
        UnitInfoStruct debugInfo = new UnitInfoStruct();
        Singleton<JSonManager>.Instance.GetUnitInfoStruct(ref debugInfo, "1", 1);
    }

    // Update is called once per frame
    void Update ()
    {
        timeCount += Time.deltaTime;
        int min, sec, mSec;
        min = (int)timeCount / 60;
        sec = (int)timeCount - min * 60;
        mSec = (int)(timeCount * 100) % 100;

        string timeString = min.ToString("00") + ":" + sec.ToString("00") + ":" + mSec.ToString("00");

        ATeamTurnTime += Time.deltaTime;
        BTeamTurnTime += Time.deltaTime;
        if(ATeamTurnTime >= 15.0f || BTeamTurnTime >= 15.0f)
        {
            ActiveTurn();
        }
    }

    /* 시간이 차면 턴을 진행.
     * 다음 턴을 반대편 팀에게 넘겨준다.
     * */
    void ActiveTurn()
    {
        if(turn == Turn.ATeam)
        {
            ATeamTurnTime = 0;
            Singleton<LevelManager>.Instance.SpawnUnitAtTurn(Team.ATeam);
            Singleton<CostManager>.Instance.GetIncome(Team.ATeam);
            turn = Turn.BTeam;
        }
        else
        {
            BTeamTurnTime = 0;
            Singleton<LevelManager>.Instance.SpawnUnitAtTurn(Team.BTeam);
            Singleton<CostManager>.Instance.GetIncome(Team.BTeam);
            turn = Turn.ATeam;
        }
    }

    private void SetNexusPosition()
    {
        Singleton<ObjectPoolScript>.Instance.NexusSetPosition(new Vector2(0, 1), Team.ATeam);
        Singleton<ObjectPoolScript>.Instance.NexusSetPosition(new Vector2(17, 1), Team.BTeam);
    }
}
